<nav class="col-md-2 d-none d-md-block bg-light sidebar">
    <div class="sidebar-sticky">
        <ul class="nav flex-column pt-4">
            <li class="nav-item">
                <a class="nav-link dashboard" href="{{ URL::to('admin/dashboard') }}">
                <span data-feather="home"></span>
                    Dashboard <span class="sr-only"></span>
                </a>
            </li>
        </ul>

        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
            <span> Catalog </span>
            <a class="d-flex align-items-center text-muted" href="#">
                <span data-feather="plus-circle"></span>
            </a>
        </h6>
        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link" href="{{ URL::to('admin/category') }}">
                <span data-feather="shopping-cart"></span>
                    Kategori
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ URL::to('admin/segment') }}">
                <span data-feather="shopping-cart"></span>
                    Segmen
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ URL::to('admin/product') }}">
                <span data-feather="shopping-cart"></span>
                    Produk
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ URL::to('admin/deal') }}">
                <span data-feather="shopping-cart"></span>
                    Promosi
                </a>
            </li>
            {{-- <li class="nav-item">
                <a class="nav-link" href="{{ URL::to('admin/pop-up') }}">
                <span data-feather="shopping-cart"></span>
                    PopUp
                </a>
            </li> --}}
        </ul>

        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
            <span> Toko  </span>
            <a class="d-flex align-items-center text-muted" href="#">
                <span data-feather="plus-circle"></span>
            </a>
        </h6>
        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link" href="{{ URL::to('admin/about-us') }}">
                <span data-feather="home"></span>
                    Tentang Kami
                </a>
            </li>
            {{-- <li class="nav-item">
                <a class="nav-link" href="{{ URL::to('admin/call-us') }}">
                <span data-feather="shopping-cart"></span>
                    Hubungi Kami
                </a>
            </li> --}}
        </ul>
        {{-- <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
            <span> Laporan </span>
            <a class="d-flex align-items-center text-muted" href="#">
                <span data-feather="plus-circle"></span>
            </a>
        </h6>
        <ul class="nav flex-column mb-2">
            <li class="nav-item">
                <a class="nav-link" href="#">
                    <span data-feather="file-text"></span>
                    Pengunjung
                </a>
            </li>
        </ul> --}}

        {{-- <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
            <span> Website  </span>
            <a class="d-flex align-items-center text-muted" href="#">
                <span data-feather="plus-circle"></span>
            </a>
        </h6>
        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link" href="#">
                <span data-feather="home"></span>
                    Konfigurasi
                </a>
            </li>
        </ul> --}}
    </div>
</nav>
<style>
@media (max-width: 768px) {
    div#navbarSupportedContent {
        background: #fff !important;
        border-bottom: 3px solid grey !important;
        width: 100% !important;
        padding: 25px 0px !important;
        padding-top: 0px !important;
    }
}
</style>
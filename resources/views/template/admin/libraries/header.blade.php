<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">


    <link rel="apple-touch-icon" sizes="57x57"          href="{{ URL::asset('favicon/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60"          href="{{ URL::asset('favicon/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72"          href="{{ URL::asset('favicon/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76"          href="{{ URL::asset('favicon/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114"        href="{{ URL::asset('favicon/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120"        href="{{ URL::asset('favicon/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144"        href="{{ URL::asset('favicon/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152"        href="{{ URL::asset('favicon/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180"        href="{{ URL::asset('favicon/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192"   href="{{ URL::asset('favicon/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32"     href="{{ URL::asset('favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96"     href="{{ URL::asset('favicon/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16"     href="{{ URL::asset('favicon/favicon-16x16.png') }}">
    <link rel="manifest" href="/manifest.json">
    <link rel="shortcut icon" href="{{ URL::asset('favicon/favicon.ico') }}" type="image/x-icon">
    
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <title> Dashboard | Foody </title>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css">
    
    <link rel="canonical" href="https://getbootstrap.com/docs/4.1/examples/dashboard/">

    <!-- Bootstrap core CSS -->
    <!-- <link href="{{ URL::asset('css/admin/bootstrap.min.css') }}" rel="stylesheet"> -->

    <!-- Custom styles for this template -->
    <link href="{{ URL::asset('css/admin/dashboard.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('css/admin/editor.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('css/admin/richtext.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" id="css-main" href="{{ URL::asset('css/admin/oneui.css') }}">
    <link rel="stylesheet" id="css-main" href="{{ URL::asset('css/admin/themes/flat.min.css') }}">
    
    <link rel="stylesheet" 
    href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">
    
    <!-- Web fonts -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">

    <!-- Page JS Plugins CSS -->
    <link rel="stylesheet" href="{{ URL::asset('js/admin/plugins/slick/slick.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('js/admin/plugins/slick/slick-theme.min.css') }}">

    {{-- <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>tinymce.init({selector:'textarea'});</script> --}}

</head>

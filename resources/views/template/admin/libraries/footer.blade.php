<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script>
    window.jQuery || document.write(
            '<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>'
        )
</script>
<script src="{{ URL::asset('js/admin/popper.min.js') }}"></script>
<script src="{{ URL::asset('js/admin/editor.js') }}"></script>
<script src="{{ URL::asset('js/admin/jquery.richtext.js') }}"></script>

<!-- OneUI Core JS: jQuery, Bootstrap, slimScroll, scrollLock, Appear, CountTo, Placeholder, Cookie and App.js -->
<script src="{{ URL::asset('js/admin/core/jquery.min.js') }}"></script>
<script src="{{ URL::asset('js/admin/core/bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('js/admin/core/jquery.slimscroll.min.js') }}"></script>
<script src="{{ URL::asset('js/admin/core/jquery.scrollLock.min.js') }}"></script>
<script src="{{ URL::asset('js/admin/core/jquery.appear.min.js') }}"></script>
<script src="{{ URL::asset('js/admin/core/jquery.countTo.min.js') }}"></script>
<script src="{{ URL::asset('js/admin/core/jquery.placeholder.min.js') }}"></script>
<script src="{{ URL::asset('js/admin/core/js.cookie.min.js') }}"></script>
<!-- <script src="{{ URL::asset('js/admin/app.js') }}"></script> -->

<!-- Page JS Plugins -->
<script src="{{ URL::asset('js/admin/plugins/summernote/summernote.min.js') }}"></script>
<script src="{{ URL::asset('js/admin/plugins/ckeditor/ckeditor.js') }}"></script>
<script src="{{ URL::asset('js/admin/plugins/simplemde/simplemde.min.js') }}"></script>

<!-- Page JS Code -->
<!-- <script>
    jQuery(function () {
        // Init page helpers (Summernote + CKEditor + SimpleMDE plugins)
        App.initHelpers(['summernote', 'ckeditor', 'simplemde']);
    });
</script> -->

<script src="https://code.iconify.design/1/1.0.3/iconify.min.js"></script>

<!-- Page Plugins -->
<script src="{{ URL::asset('js/admin/plugins/chartjs/Chart.min.js') }}"></script>

<!-- Page JS Code -->
<!-- <script src="{{ URL::asset('js/admin/pages/frontend_features.js') }}"></script> -->
<!-- <script>
    jQuery(function () {
        // Init page helpers (Appear + CountTo plugins)
        App.initHelpers(['appear', 'appear-countTo']);
    });
</script> -->

<!-- Icons -->
<script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
<script>
    feather.replace()
</script>

<!-- Graphs -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>

<style>
.sidebar-sticky {
    height: 100% !important;
    overflow: hidden;
}
a.nav-link.dashboard {
    font-size: 14px;
    font-weight: 100;
}
a.nav-link:hover {
    color: #fff;
}
.navbar-brand {
    background-color: #fff !important;
    box-shadow: none !important;
}
main.py-4 {
    padding: 0 !important;
    margin: 0 auto;
    padding-top: 30px !important;
    background: #fff !important;
}
.sidebar .nav-link.active {
    color: #ffffff;
}
.sidebar .nav-link {
    font-weight: 500;
    color: #a4a4a4;
    font-size: 14px;
}
nav.navbar.navbar-expand-md.navbar-light.bg-white.shadow-sm {
    position: fixed;
    width: 100%;
    z-index: 999 !important;
}
h6.sidebar-heading.d-flex.justify-content-between.align-items-center.px-3.mt-4.mb-1.text-muted {
    padding: 0px 20px !important;
    border-left: 1px solid silver !important;
    font-size: 22px !important;
    font-weight: 500 !important;
    margin: 15px 0px!important;
    color: #fff !important;
}
nav.col-md-2.d-none.d-md-block.bg-light.sidebar {
    background: #191919 !important;
    position: fixed;
    overflow: auto;
    margin-top: 0px;
    box-shadow: 1px 1px 3px 1px #000;
}
li.nav-item.dropdown {
    position: absolute;
    right: 25px;
    top: 10px;
    display: inline-block;
}
nav.navbar.navbar-expand-md.navbar-light.bg-white.shadow-sm {
    height: 60px;
    position: fixed;
    width: 100%;
    z-index: 999 !important;
}
a.navbar-brand {
    position: absolute;
    left: 85px;
}
.dropdown-menu.dropdown-menu-right.show {
    text-align: right;
    margin: 0px 0px;
}

@media (max-width: 768px) {
    li.nav-item.dropdown {
        position: relative !important;
    }
    a#navbarDropdown {
        position: relative;
        left: 25px;
        padding: 25px 0px;
    }
    .navbar-nav .dropdown-menu {
        position: absolute !important;
        right: -110px !important;
        top: 10px !important;
        transition: 1.5s;
        background: #afafaf;
    }
    a.navbar-brand {
        position: relative;
        left: 0px;
    }
    a.dropdown-item {
        color: #fff;
    }
    .dropdown-menu.dropdown-menu-right.show {
        text-align: left;
        margin: 0px -25px;
    }
}


body.body-admin {
    zoom: 85% !important;
}
.header {
    margin-top: 0px;
    position: fixed;
    margin-bottom: 0;
    background-color: rgb(255, 255, 255);
    z-index: 999 !important;
}
ul.sub-menu {
    z-index: 999 !important;
}
div#primary {
    z-index: 0 !important;
    display: flex;
}
li.menu-item.menu-item-type-post_type.menu-item-object-ic_mega_menu.menu-item-865 {
    width: 100%;
    font-size: 13px;
}
h3, .h3 {
    font-size: 17px;
}
.col-md-3.header-left {
    text-align: center;
}
.header-container {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    z-index: 999;
    background: #fff;
}
.vc_row.wpb_row.vc_row-fluid.vc_custom_1478142456781.vc_row-has-fill {
    margin-top: 130px;
}
.header-container {
    border-bottom: 5px solid #8db359;
}
p {
    margin-bottom: 0;
    -webkit-font-smoothing: antialiased;
}
nav.navbar.navbar-expand-lg.navbar-light.bg-light.custom {
    display: none;
}
a:hover {
    text-decoration: none;
}
@media only screen and (max-width: 767px) {
    nav.navbar.navbar-expand-lg.navbar-light.bg-light.custom {
        display: flex;
        background: #fff !important;
    }
    nav#menu {
        display: none;
    }
    #menu .menu__container > li, #menu .menu__container > ul > li {
        position: relative;
        display: inline;
    }
    .container.content {
        padding-top: 80px !important;
    }
    .product-countdown.col-md-6 {
        padding: 30px;
        margin: 10px;
    }
    .vc_row.wpb_row.vc_row-fluid.vc_custom_1478142456781.vc_row-has-fill {
        margin-top: 0px;
    }
    .col-md-3.header-left {
        padding-top: 35px;
        display: none;
    }
    ul.sub-menu {
        margin: 0 auto;
        height: 235px;
        overflow: auto;
        overflow-x: hidden;
        padding: 0;
        list-style: none !important;
        position: relative;
    }
    .vc_row {
        margin-left: 0px !important;
        margin-right: 0px !important;
    }
    .vc_custom_1471503185499 {
        padding-top: 10px !important;
        padding-right: 0px !important;
        padding-bottom: 0px !important;
        padding-left: 15px !important;
        margin: 0 auto !important;
        margin-bottom: -15px !important;
    }
    .vc_custom_1471503017467 {
        padding-top: 0px !important;
        padding-right: 0px !important;
        padding-bottom: 0px !important;
        padding-left: 0px !important;
    }
    .wpb_single_image img {
        height: auto;
        max-width: 100%;
        vertical-align: top;
    }
}
</style>
<script>
// var editor = $("#placeHolder").Editor();

// editor('createMenuItem', {"text": "TouchGlasses", //Text replaces icon if its not available
//     "icon":"fa fa-glass", //This is a Font-Awesome Icon
//     "tooltip": "Touch Glasses",
//     "custom": function(button, parameters){
//                 //Your Custom Function.
//                 alert("Cheers!!!");
//             },
//     "params": {'option1':"value1"} //Any custom parameters you want to pass
//                                     //to your custom function.
// });

// var viewMode = getCookie("view-mode");
// if(viewMode == "desktop"){
//     viewport.setAttribute('content', 'width=1024');
// }else if (viewMode == "mobile"){
//     viewport.setAttribute('content', 'width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no');
// }
// </script>

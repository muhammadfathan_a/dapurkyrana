<footer class="footer light_style footer-01 our-footer" id="contact">
    <div class="container">
        <div class="row">
            <div class="col-md-4 footer-c1">
                <div id="text-3" class="widget widget_text"><h3 class="widget-title"> Kontak Kami  </h3>		
                    <div class="textwidget">
                        <p><ion-icon name="pin" class="ion-ios-home" id="ico"></ion-icon>
                            2/45 Tower Street, New York, USA
                        </p>
                        {{-- <p><ion-icon name="call" class="ion-ios-telephone" id="ico"></ion-icon>
                            0012 678 8899
                        </p> --}}
                        <p><ion-icon name="mail-open" class="ion-ios-email" id="ico"></ion-icon>
                            contact@organie.com
                        </p>
                        <p><ion-icon name="link" class="ion-android-globe" id="ico"></ion-icon>
                            <a href="#" style="color: #7db5bd;">www.dapurkyrana.com</a>
                        </p>
                    </div>
                </div>
                {{-- <div class="footer-social">
                    <a class="hint--top hint--bounce hint--success" aria-label="Facebook" href="https://facebook.com">
                        <i class="fa fa-facebook"></i>
                    </a>
                    <a class="hint--top hint--bounce hint--success" aria-label="Twitter" href="https://twitter.com">
                        <i class="fa fa-twitter"></i>
                    </a>
                    <a class="hint--top hint--bounce hint--success" aria-label="Pinterest"
                    href="https://pinterest.com">
                        <i class="fa fa-pinterest"></i>
                    </a>
                    <a class="hint--top hint--bounce hint--success" aria-label="Instagram" href="https://www.instagram.com">
                        <i class="fa fa-instagram"></i>
                    </a>
                </div> --}}
            </div>
            <div class="col-md-2 footer-c2">
                <div id="insight-core-bmw-2" class="widget insight-core-bmw">
                    <h3 class="widget-title">
                        Kunjungi Toko
                    </h3>
                    <div class="menu-footer-infomation-container">
                        <ul id="menu-footer-infomation" class="menu">
                            <li id="menu-item-855" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-855">
                                <a class="foot-link" href="https://www.tokopedia.com" target="_blank"> Tokopedia </a>
                            </li>
                        </ul>
                    </div>
                    {{-- <h3 class="widget-title">
                        Hubungi Kami
                    </h3>
                    <div class="menu-footer-infomation-container">
                        <ul id="menu-footer-infomation" class="menu">
                            <li id="menu-item-856" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-856">
                                <a class="foot-link" href="https://wa.me/087784449401?text=Assalammualaikum..%20Saya%20ingin%20membuat%20pesanan" target="_blank"> What's App </a>
                            </li>
                        </ul>
                    </div> --}}
                </div>
            </div>
            <div class="col-md-6 footer-c4">
                <div id="insight_instagram-1" class="widget widget_insight_instagram">
                    <h3 class="widget-title"> Lihat Feed Instagram Kami </h3>
                    <div class="insight-instagram-wrap">
                        <div class="insight-instagram-row row">
                            @foreach ($insta_pic as $item)
                            <div class="item col-xs-3">
                                <a href="//instagram.com/dapurkyrana" target="_blank">
                                    {{-- <div class="item-info">
                                        <span class="likes"> 3 </span>
                                        <span class="comments"> 0 </span>
                                    </div> --}}
                                    <img src="{{ $item }}" alt="Instagram" class="item-image insta" />
                                </a>
                            </div>
                            @endforeach
                        </div>
                        {{-- <div class="insight-instagram-row row">
                            <div class="item col-xs-4">
                                <a href="//instagram.com/p/BLuab7xh4_F/" target="_blank">
                                    <div class="item-info">
                                        <span class="likes">0</span>
                                        <span class="comments">0</span>
                                    </div>
                                    <img src="//scontent-sin6-2.cdninstagram.com/vp/1fa6ab3601efbbcaab9ea9c7adf1cee9/5DE76E40/t51.2885-15/e35/s150x150/14733353_1084311211689605_3495251506540576768_n.jpg?_nc_ht=scontent-sin6-2.cdninstagram.com" alt="Instagram" class="item-image" />
                                </a>
                            </div>
                        </div> --}}
                    </div>
                </div>
            </div>
        </div> <!-- /.row -->
    </div><!-- /.wrapper -->
</footer><!-- /.footer -->
<div class="copyright light_style our-foot">
    <div class="container">
        <div class="copyright-container">
            <div class="row row-xs-center">
                <div class="col-md-6 copyright-left">
                    <a href="{{ URL::to('') }}">
                        Copyright © 2019 www.dapurkyrana.com
                    </a>
                </div>
                <div class="col-md-6 copyright-right">
                    <ul id="menu-copyright-menu" class="copyright-menu">
                        <li id="menu-item-853" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-853">
                            {{-- <a href="#">Privacy Policy</a> --}}
                        </li>
                        <li id="menu-item-854" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-854">
                            <a href="https://instagram.com/muhammadfathan_a">AkhtarFath Work's</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
</div><!-- /.site -->
<style>
    .our-footer {
        padding: 50px 55px !important;
        padding-bottom: 0px !important;
        {{--  background: #000000;
        color: #fff !important;  --}}
    }
    .our-foot {
        padding: 0px 55px !important;
        {{--  background: #000000;
        color: #fff !important;  --}}
    }
    .our-foot a {
        {{--  color: #fff !important;  --}}
    }
    h3.widget-title {
        {{--  color: #fff !important;  --}}
    }
    #ico {
        font-size: 17px;
        padding: 0px 15px;
        zoom: 100%;
        color: #fbbf34;
    }
    img.item-image.insta {
        border: 3px solid #fbbf34;
        width: 145px;
        height: 145px;
        padding: 0px;
        margin: 0px;
        border-radius: 5px;
    }
    .foot-link:hover {
        color: #333 !important;
        display: block !important;
    }
    .footer .widget ul li a {
        position: none !important;
    }
</style>

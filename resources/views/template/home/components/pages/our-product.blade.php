@php error_reporting(1) @endphp
<div class="vc_row wpb_row vc_row-fluid vc_custom_1478162727807 our-product" id="product" style="margin-top: 0px !important; padding-top: 15px !important;">
<div class="wpb_column vc_column_container vc_col-sm-12">
<div class="vc_column-inner ">
    <div class="wpb_wrapper">
        <div class="insight-title text-center has-bg text-uppercase font-primary font-40 ofw-900 ofs-normal">
            <h2 class="text-center"> Produk Kami </h2>
        <div class="sub-title primary-color"> - Produk Unggulan - </div>
    </div>
    <div class="insight-product-grid insight-woo default">
        <div class="insight-grid-filter">
            {{--  <p> Menampilkan untuk kategori </p>  --}}
            <hr>
            <ul data-option-key="filter">
                <li>
                    {{-- <a class="active" href="#filter" data-option-value=".product" style="margin: 0 10px;"> --}}
                    @if (!$_GET['category'])
                        <a href="{{ URL::to('home#product') }}" data-option-value=".product"
                        class="cat-link active cat-prod" style="margin: 0 10px;">
                            All
                        </a>
                    @else
                        <a href="{{ URL::to('home#product') }}" data-option-value=".product"
                        class="cat-link cat-prod" style="margin: 0 10px;">
                            All
                        </a>
                    @endif
                    @if ($_GET['category'])
                        @foreach ($category as $cat)
                            @if ($cat->nm_category == $_GET['category'])
                                @php $active = 'active' @endphp
                                <a href="{{ URL::to('?category='.$cat->nm_category) }}"
                                    data-option-value=".product_cat-{{ $cat->nm_category }}"
                                    class="cat-link {{ $active }}"
                                    style="margin: 0 10px;">
                                    {{ $cat->nm_category }}
                                </a>
                            @else
                            <a href="{{ URL::to('?category='.$cat->nm_category) }}"
                                data-option-value=".product_cat-{{ $cat->nm_category }}"
                                class="cat-link"
                                style="margin: 0 10px;" class="cat-link">
                                {{ $cat->nm_category }}
                            </a>
                            @endif
                        @endforeach
                    @else
                        @foreach ($category as $cat)
                        <a href="{{ URL::to('?category='.$cat->nm_category) }}"
                            data-option-value=".product_cat-{{ $cat->nm_category }}"
                            class="cat-link"
                            style="margin: 0 10px;" class="cat-link">
                            {{ $cat->nm_category }}
                        </a>
                        @endforeach
                    @endif
                </li>
            </ul>
            <hr>
        </div>
        <div class="columns-4">
            <div class="products row" style="margin: 0px !important;">
                @if ($_GET['category'])
                    @foreach ($category as $cat)
                        @foreach ($product as $item)
                            @foreach ($image as $img)
                                @if ($item->id_category == $cat->id_category)
                                    @if ($item->id_image == $img->id_image)
                                        @if ($cat->nm_category == $_GET['category'])
                                        <div class="loop-product product style-01 col-md-4 post-1216 type-product status-publish has-post-thumbnail product_cat-skin-care product_tag-citronella product_tag-herbani product_tag-natural product_tag-skin-care product_tag-soap first instock shipping-taxable purchasable product-type-simple text-center">
                                            <div class="loop-product-inner">
                                                <div class="thumb">
                                                    <h1 class="woocommerce-loop-product__title h5 text-center pb-3">
                                                        {{ $item->nm_product }}
                                                    </h1>
                                                    <img width="300" height="300"
                                                        src="{{ URL::asset($img->nm_image) }}"
                                                        alt="product-img" class="img-product"
                                                    />
                                                </div>
                                                <button class="btn btn-warning text-center mb-2 text-light" 
                                                style="border-radius: 100px;" data-toggle="modal" data-target="#modal-product">
                                                    <ion-icon name="radio-button-on" 
                                                    style="color: #fff;font-size: 19px;padding: 0px !important;">
                                                    </ion-icon>
                                                </button>
                                                <div class="product-info" style="padding-top: 15px;">
                                                    <h5 class="woocommerce-loop-product__title">
                                                        Tokopedia
                                                    </h5>
                                                    <span class="price">
                                                        <span class="woocommerce-loop-product__title">
                                                            <span class="woocommerce-loop-product__title">
                                                                Rp
                                                            </span>
                                                            {{ $item->price }}
                                                        </span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                        @if ($cat->nm_category != $_GET['category'])
                                            @if ($loop->parent->first)
                                                <div class="text-center undified{{ $no }}" style="margin: auto;">
                                                    <p style="font-size: 17px;"> Produk untuk kategori
                                                        <b>{{ $_GET['category'] }}</b>, tidak ditemukan.
                                                    </p>    
                                                </div> 
                                            @endif
                                        @endif  
                                    @endif
                                @endif
                            @endforeach
                        @endforeach
                    @endforeach
                @endif

        {{--  Batassssssssssssssssssssssssssssssssssssssssssssssssssssss Categoryyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy  --}}

                @if (!$_GET['category'])
                @foreach ($product as $item)
                    @foreach ($category as $cat)
                        @foreach ($image as $img)
                            @if ($item->id_category == $cat->id_category)
                                @if ($item->id_image == $img->id_image)
                                    <div class="loop-product product style-01 col-md-4 post-1216 type-product status-publish has-post-thumbnail product_cat-skin-care product_tag-citronella product_tag-herbani product_tag-natural product_tag-skin-care product_tag-soap first instock shipping-taxable purchasable product-type-simple">
                                        <div class="loop-product-inner text-center" style="background: #fff;">
                                            <div class="thumb">
                                                <h1 class="woocommerce-loop-product__title h5 text-center pb-3">
                                                    {{ $item->nm_product }}
                                                </h1>
                                                <img width="300" height="300"
                                                    src="{{ URL::asset($img->nm_image) }}"
                                                    alt="product-img" class="img-product"
                                                />
                                            </div>
                                            <button class="btn btn-warning text-center mb-2 text-light" style="border-radius: 100px;" data-toggle="modal" data-target="#modal-product-{{ $item->id_product }}">
                                                <ion-icon name="radio-button-on" 
                                                style="color: #fff;font-size: 19px;padding: 0px !important;">
                                                </ion-icon>
                                            </button>
                                            <div class="product-info" style="padding-top: 15px;">
                                                <h5 class="woocommerce-loop-product__title">
                                                    Tokopedia
                                                </h5>
                                                <span class="price">
                                                    <span class="woocommerce-loop-product__title">
                                                        <span class="woocommerce-loop-product__title">
                                                            Rp
                                                        </span>
                                                        {{ $item->price }}
                                                    </span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endif
                        @endforeach
                    @endforeach
                @endforeach
                @endif

            </div>
        </div>
    </div>
</div>

@if (!$_GET['category'])
    <div class="container">
        <nav class="pagination-pack">
            <a class="page" href="#">{{ $product->links() }}</a>
        </nav>
    </div>
@endif

@foreach ($product as $item)
@foreach ($category as $cat)
@foreach ($segment as $seg)
    @foreach ($image as $img)
        @if ($item->id_category == $cat->id_category)
        @if ($item->id_image == $img->id_image)
            @if ($item->id_segment == $seg->id_segment)
            <!-- Modal -->
            <div class="modal fade" id="modal-product-{{ $item->id_product }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="background: rgba(0, 0, 0, 0.85);">
                <div class="modal-dialog" role="document" style="max-width: 917px;">
                    <div class="modal-content" style="background: transparent; border: none;">
                        <div class="modal-header" style="border: none;">
                            {{-- <span class="text-light"> {{ $item->updated_at  }} </span> --}}
                        </div>
                        <div class="modal-body rounded" style="background: #000; padding: 0px;">
                            <div class="form-row">
                                <div class="col-md-6 p-5 pl-4 pr-4 rounded">
                                    <img src="{{ URL::asset($img->nm_image) }}"
                                        alt="product-img" class="img-product" 
                                        style="height: auto !important;"/>
                                </div>
                                <div class="col-md-6 p-5 rounded" style="background: #fff;">
                                    <p style="padding: 0px !important; margin-bottom: 0px !important;">
                                        <h5 class="modal-title text-dark" id="exampleModalLabel"> 
                                            "{{ $item->nm_product }}" 
                                        </h5>
                                        <hr>
                                        <b> Aplikasi : </b> Tokopedia
                                        {{-- {{ $seg->nm_segment }} --}}
                                        <br>
                                        <b> Kategori : </b> {{ $cat->nm_category }}
                                        <br>
                                        <div class="col-desc">
                                            <b> Deskripsi : </b> 
                                            <br>
                                            "{{ $item->description }}"-
                                        </div>
                                        <div class="btn-checkout d-inline-block">
                                            <a class="btn btn-success text-light d-inline-block" id="btn-btn-btn" 
                                            href="#" style="width: 85%; margin: 0 auto;">         
                                                <b> Harga : </b> Rp. {{ number_format($item->price,0) }}
                                            </a>
                                            <button class="btn btn-secondary d-inline-block" id="btn-btn-btn" data-dismiss="modal"> X </button> 
                                        </div>
                                    </p>
                                </div>
                            </div>
                        </div>
                        {{-- <div class="modal-footer" style="background: transparent; border: none;">
                            <div class="bungkus-gan" style="width: 100%">
                                <button class="btn btn-secondary" id="btn-btn-btn" data-dismiss="modal"> Batalkan </button> 
                                <a class="btn btn-success text-light" id="btn-btn-btn" href="/checkout" style="width: 100%"> 
                                    Checkout 
                                </a>
                            </div>
                        </div> --}}
                    </div>
                </div>
            </div>
            @endif
        @endif
        @endif
    @endforeach
@endforeach
@endforeach
@endforeach

<style>
}
div#product {
    margin-top: 55px !important;
}
.woocommerce .products .loop-product.style-01 .loop-product-inner, .insight-woo .products .loop-product.style-01 .loop-product-inner, .woocommerce-page .products .loop-product.style-01 .loop-product-inner {
    max-height: max-content;
    width: 100%;
    padding: 25px 10px;
    border: 1px solid #f4f4f4;
    position: relative;
    border-radius: 0px;
    transition: all 0.5s;
    height: 420px;
    box-shadow: 15px 15px 0px -5px #00000012;
}
.woocommerce .products .loop-product.style-01, .insight-woo .products .loop-product.style-01, .woocommerce-page .products .loop-product.style-01 {
    outline: none;
    margin-bottom: 30px;
    height: auto;
}
nav.pagination-pack {
    margin: auto;
    padding: 0;
    display: table;
}
{{--  img.img-product {
    margin: 0 !important;
    padding: 0 !important;
    height: 285px !important;
    width: 100%;
}  --}}
img.img-product:hover {
    opacity: 0.8;
    cursor: pointer;
    transition: 1s;
}
.woocommerce .products .loop-product.style-01 .loop-product-inner, .insight-woo .products .loop-product.style-01 .loop-product-inner, .woocommerce-page .products .loop-product.style-01 .loop-product-inner {
    width: 100%;
    height: 100%;
    padding: 0px 0px 30px 0px;
    border: 1px solid #f4f4f4;
    position: relative;
    transition: all 0.5s;
}
li.page-item {
    display: inline-block;
}
.products.row {
    margin: 0px 25px;
}
a.cat-link.active {
    padding: 5px 30px;
    margin: 5px 10px !important;
    color: #ffffff !important;
    display: inline-block !important;
    outline: none !important;
    border: none !important;
    background: linear-gradient(35deg, #fdbf34, #6dbfca) !important;
}
.loop-product-inner:hover {
    box-shadow: -15px 15px 0px -5px #00000012 !important;
    {{-- border: none !important; --}}
    {{-- outline: none !important; --}}
    cursor: pointer !important;
    transition: 3s;
}
a.cat-link {
    padding: 3px 15px;
    margin: 5px 10px !important;
    display: inline-block !important;
    background: #fff !important;
    border: none !important;
}
a.cat-link.active:hover {
    background: #e2a932 !important;
    color: #fff !important;
    transition: 0.75s;
}
a.cat-link:hover {
    transition: 0.25s;
    color: grey !important;
    border: none !important;
    background: #e4e4e4 !important;
    transition: 0.75s;
}
.sub-title.primary-color {
    color: #588188 !important;
    font-weight: 700 !important;
    margin-top: 10px !important;
}
.thumb {
    padding: 0px !important;
    /* box-shadow: -10px 10px 0px 0px #0000001a; */
    margin: 25px !important;
    border-radius: 10px !important;
}
.loop-product-inner {
    /* border-left: 5px solid #fbbf34 !important;
    border-bottom: 5px solid #6dbfca !important;
    border-right: 5px solid #6dbfca !important;
    border-top: 5px solid #febc34 !important; */
    border: 5px solid #fff !important;
    border-radius: 10px !important;
    padding: 0px !important;
}
.insight-grid-filter {
    /* background: #6dbfca; */
    background: #fff;
}
.img-product {
    z-index: 0 !important;
    display: flex;
    border-radius: 10px !important;
}
.product-info {
    padding-bottom: 45px;
    padding-top: 0px;
}
.bungkus-gan {
    background: #ffff;
    padding: 13px;
    margin: 0px;
    border-radius: 5px;
    zoom: 85%;
}
#btn-btn-btn {
    margin: 0px 5px;
}
.col-desc {
    margin-top: 75px;
}
img.img-product {
    margin: 0 !important;
    padding: 0 !important;
    height: 225px !important;
    width: 100%;
}
.modal-footer {
    padding: 15px 0px !important;
}
/* .row, .insight-carousel .slick-track {
    display: flex;
    flex-wrap: wrap-reverse;
    margin-left: -0.9375rem;
    margin-right: -0.9375rem;
} */
.vc_custom_1478162727807 {
    padding-top: 75px !important;
}
.btn-checkout {
    position: absolute;
    bottom: 15px;
    right: 10px;
    left: 10px;
    text-align: center;
}
.col-md-6.p-5.pl-4.pr-4.rounded {
    padding: 10% 50px !important;
}
@media (max-width: 768px) {
    h2.text-center {
        font-size: 28px !important;
    }
    a.cat-link {
        display: inline-block !important;
    }
    .sub-title.primary-color {
        font-size: 17px !important;
    }
    img.item-image.insta { 
        height: 75px !important;
    }
}
</style>
<script>
    $('.cat-link').on('click', function() {
        $('.cat-link').removeClass('active');
        $(this).addClass('active');

        let kategori = $(this).html();

    });
</script>

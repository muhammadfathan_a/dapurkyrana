<div class="vc_row wpb_row vc_row-fluid vc_custom_1478253001333 our-blog" id="blog">
    <div class="wpb_column vc_column_container vc_col-sm-12">
        <div class="vc_column-inner ">
            <div class="wpb_wrapper">
                <div class="insight-title text-center has-bg text-uppercase font-primary font-40 ofw-900 ofs-normal">
                    <h2 class="main-title"> Baca Blog </h2>
                    <div class="sub-title primary-color">
                        - Tetap <i> Up To Date </i> Bersama Kami -
                    </div>
                </div>
                <div class="insight-blog row vc_custom_1478256066483 style-01">
                    <div class="col-md-6 left">
                        <div class="blog-classic-style post-880 post type-post status-publish format-standard has-post-thumbnail hentry category-other category-vegetables tag-food tag-healthy tag-natural">
                            <div class="post-thumbnail">
                                <a href="#">
                                    <img src="http://herbani.co.id/wp-content/uploads/2016/10/cb3991cc1dc4a38f4e4a3b72a5632904-870x470.jpg" class="attachment-insight-post-full size-insight-post-full wp-post-image" alt=""/>
                                </a>
                            </div>
                            <div class="entry-desc">
                                <div class="entry-meta nd-font">
                                    <span class="posted-on">
                                        <time class="entry-date published updated" datetime="2016-10-18T01:59:37+00:00">
                                            October 18, 2016
                                        </time>
                                    </span>
                                </div>
                                <a href="#">
                                    <h4 class="entry-title nd-font">
                                        Why You Should Eat Avocados
                                    </h4>
                                </a>
                                <div class="entry-content">
                                    It’s no secret that I love to eat.&hellip;
                                </div>
                                <div class="entry-more">
                                    <a href="http://herbani.co.id/2016/10/18/why-you-should-eat-avocados/">
                                        / Read more
                                    </a>
                                </div>
                            </div>
                        </div> <!-- #post-## -->
                    </div>
                    <div class="col-md-6 right">
                        <div class="blog-classic-style style-02 post-227 post type-post status-publish format-standard has-post-thumbnail hentry category-other category-vegetables tag-fruits tag-green tag-vegetables">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="post-thumbnail">
                                        <a href="#">
                                            <img src="http://herbani.co.id/wp-content/uploads/2016/10/gypsy-juice-organic-skincare-370x250.jpg" class="attachment-370x250 size-370x250 wp-post-image" alt="" />
                                        </a>
                                    </div>
                                </div>
                                <div class="entry-desc col-md-6">
                                    <div class="entry-meta nd-font">
                                        <span class="posted-on">
                                            <time class="entry-date published updated" datetime="2016-10-21T04:27:56+00:00">
                                                October 21, 2016
                                            </time>
                                        </span>
                                    </div>
                                    <a href="#">
                                        <h5 class="entry-title nd-font">
                                            Become Heart Healthy in 5 Steps
                                        </h5>
                                    </a>
                                    <div class="entry-content">
                                        Improving your diet lowers your risk for heart&hellip;
                                    </div>
                                    <div class="entry-more">
                                        <a href="#">
                                            / Read more
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div><!-- #post-## -->
                        <div class="blog-classic-style style-02 post-231 post type-post status-publish format-standard has-post-thumbnail hentry category-other category-vegetables tag-food tag-organic-store tag-vegetables">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="post-thumbnail">
                                        <a href="#">
                                            <img src="http://herbani.co.id/wp-content/uploads/2016/10/bbf9c528991631.55dcc47c30b2b-370x250.jpg" class="attachment-370x250 size-370x250 wp-post-image" alt="" />
                                        </a>
                                    </div>
                                </div>
                                <div class="entry-desc col-md-6">
                                    <div class="entry-meta nd-font">
                                        <span class="posted-on">
                                            <time class="entry-date published updated" datetime="2016-10-21T04:29:18+00:00">
                                                October 21, 2016
                                            </time>
                                        </span>
                                    </div>
                                    <a href="#">
                                        <h5 class="entry-title nd-font">
                                            Feeding kids organic food
                                        </h5>
                                    </a>
                                    <div class="entry-content">
                                        Choosing organic meat and dairy for your kids&hellip;
                                    </div>
                                    <div class="entry-more">
                                        <a href="#">
                                            / Read more
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div><!-- #post-## -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
.vc_row.wpb_row.vc_row-fluid.vc_custom_1478253001333.our-blog {
    margin: 0px 40px;
}
.insight-blog .blog-classic-style:first-child .post-thumbnail:before {
    display: block;
    content: '';
    border: 5px solid #fbbf34 !important;
    position: absolute;
    z-index: 1;
    top: -15px;
    right: -15px;
    bottom: 15px;
    left: 15px;
}
</style>

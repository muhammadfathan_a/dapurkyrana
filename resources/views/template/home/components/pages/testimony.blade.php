<div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid vc_custom_1478251301734 vc_row-has-fill">
    <div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner ">
        <div class="wpb_wrapper">
            <div class="insight-testimonials style-01" id="insight-testimonials-5d416d6ca65b0">
                <div class="item">
                    <div class="photo">
                        <img width="80" height="80" src="http://herbani.co.id/wp-content/uploads/2016/10/test-01.jpg" class="attachment-full size-full" alt="" />
                    </div>
                    <h4 class="title"> The best Organic store! </h4>
                    <div class="text nd-font ">
                        Thank you for all the amazing products you deliver each week... I’ve been telling everyone about your greatest online health food shop!
                    </div>
                    <div class="info">
                        <div class="rate">
                            <span class="light ion-android-star"></span>
                            <span class="light ion-android-star"></span>
                            <span class="light ion-android-star"></span>
                            <span class="light ion-android-star"></span>
                            <span class="ion-android-star-outline"></span>
                        </div>
                        <div class="author">
                            <span class="name">Jerry Hansen</span>
                            <span class="tagline nd-font">Webdesign</span>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="photo">
                        <img width="60" height="60" src="http://herbani.co.id/wp-content/uploads/2016/11/testi_03.jpg" class="attachment-full size-full" alt="" />
                    </div>
                        <h4 class="title"> The best Organic store! </h4>
                    <div class="text nd-font ">
                        Thank you for all the amazing products you deliver each week... I’ve been telling everyone about your greatest online health food shop!
                    </div>
                    <div class="info">
                        <div class="rate">
                            <span class="light ion-android-star"></span>
                            <span class="light ion-android-star"></span>
                            <span class="light ion-android-star"></span>
                            <span class="light ion-android-star"></span>
                            <span class="light ion-android-star"></span>
                        </div>
                        <div class="author">
                            <span class="name">Doe John</span>
                            <span class="tagline nd-font">Dev</span>
                        </div>
                    </div>
                </div>
            </div>
            <script>
            jQuery( document ).ready( function( $ ) {
            jQuery( "#insight-testimonials-5d416d6ca65b0" ).slick( {
                slidesToShow: 1,
                slidesToScroll: 1,

                                autoplay: true,

                                dots: false,

                                arrows: true,

                infinite: true,
                responsive: [
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                ]
            } );
            } );
            </script>
        </div>
    </div>
</div>

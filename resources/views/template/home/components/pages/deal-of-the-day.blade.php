<div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid vc_custom_1478142456781 vc_row-has-fill" id="home">
    <div class="wpb_column vc_column_container vc_col-sm-12" id="courossel">
        <div class="vc_column-inner vc_custom_1478142840868">
            <div class="wpb_wrapper">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        @foreach ($deal as $deals)
                         <li
                            data-target="#carouselExampleIndicators"
                            data-slide-to="{{ $loop->iteration }}"
                            class="{{ $loop->first ? 'active' : '' }}">
                        </li>
                        @endforeach
                    </ol>
                    <div class="carousel-inner" role="listbox">
                    @foreach ($deal as $deals)
                    <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
                        <div class="insight-countdown-product" id="insight-countdown-product-5d416d6c97e07">
                            <div class="item row row-xs-center">
                                <div class="special-bg">
                                    <div class="inner"></div>
                                </div>
                                    <a class="col-md-6">
                                    <div class="product-image">
                                        <div class="product-price nd-font">
                                            <span class="mb-1" style="font-size: 12px !important;"> -Sekarang- </span>
                                            <ins>
                                                <span class="woocommerce-Price-amount amount">
                                                <span class="woocommerce-Price-currencySymbol">
                                                    Rp
                                                </span> {{ number_format($deals->price,0) }} </span>
                                            </ins>
                                        </div>
                                        <div class="img-product deal-bungkus">
                                        @foreach ($image as $img)
                                        @if ($deals->id_image == $img->id_image)
                                             {{-- <img class="deal" width="1500" height="1500" src="{{ URL::asset($img->nm_image) }}" class="attachment-full size-full" alt="" sizes="(max-width: 1500px) 100vw, 1500px" /> --}}
                                        @endif
                                        @endforeach
                                        </div>
                                    </div>
                                    </a>
                                <div class="product-countdown col-md-6">
                                    <h2 class="title"> - {{ $deals->title_deal }} </h2>
                                    {{-- <img src="{{ URL::asset('image/keju.png') }}" alt="" class="keju"> --}}
                                <div class="bungkus-kata">
                                    <div class="product-name nd-font">
                                       <i> "{{ $deals->subtitle_deal }}" </i>
                                    </div>
                                    <div class="product-desc">
                                        {{ $deals->description }}
                                    </div>
                                        <p class="product woocommerce add_to_cart_inline " style="border:4px solid #ccc; padding: 12px;">
                                        <ins>
                                            <span class="woocommerce-Price-amount amount">
                                                <span class="woocommerce-Price-currencySymbol">
                                                    Rp
                                                </span>
                                                12.00
                                            </span>
                                        </ins>
                                        <div class="woocommerce_loop_add_to_cart_span hint--top hint--rounded hint--bounce hint--success">
                                        {{-- <a href="#" class="selengkapnya">
                                            Selengkapnya...
                                        </a> --}}
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
                <script>
                    jQuery( document ).ready( function() {
                    jQuery( '.product-countdown-timer' ).each( function() {
                    var thisID = jQuery( this ).attr( 'id' );
                    var target = new Date( jQuery( this ).text() );
                    var current = new Date();
                    if ( target.getTime() < current.getTime() ) {
                        document.getElementById( thisID ).innerHTML = '';
                        return;
                    }

                    countdown.resetLabels();
                    countdown.setLabels(
                        ' millisecond| <span>-Sec-</span></span>| <span>-Min-</span> | <span>-Hour-</span> | <span>-Day-</span> | <span>week</span> | <span>month</span> | <span>year</span> | <span>decade</span> | <span>century</span> | <span>millennium</span>',
                        ' milliseconds| <span>-Secs-</span> | <span>-Mins-</span> | <span>-Hours-</span> | <span>-Days-</span> | <span>weeks</span> | <span>months</span> | <span>years</span> | <span>decades</span> | <span>centuries</span> | <span>millennia</span>',
                        '',
                        '',
                        '',
                        function( n ) {
                            if ( n < 10 ) {
                                return '0' + n.toString();
                            }
                            return n.toString();
                        } );
                    countdown(
                        target,
                        function( ts ) {
                            if ( ts.hours === 0 ) {
                                ts.hours = '0';
                            }
                            if ( ts.minutes === 0 ) {
                                ts.minutes = '0';
                            }
                            if ( ts.seconds === 0 ) {
                                ts.seconds = '0';
                            }
                            if ( ts.days === 0 ) {
                                ts.days = '0';
                            }
                            document.getElementById( thisID ).innerHTML = ts.toHTML( 'div' );
                        },
                        countdown.DAYS + countdown.HOURS + countdown.MINUTES + countdown.SECONDS
                    );
                    } );
                    } );
                </script>
            </div>
        </div>
    </div>
</div>
<style>
@foreach ($image as $img)
    @if ($deals->id_image == $img->id_image)
        div#home {
            margin-top: 15px;
            padding: 75px 25px;
            /* background: #94d6e0 !important; */
            background-image: url("{{ URL::asset('image/header-background.png') }}");
            padding-bottom: 0px;
            background-size: 100%;
            background-repeat: repeat-y;
            background-position: center;
            background-attachment: fixed;
        }
    @endif
@endforeach
div#insight-countdown-product-5d416d6c97e07 {
    {{-- background: #fff !important; --}}
}
.insight-countdown-product .item .product-countdown {
    width: 100%;
    overflow: hidden;
    text-align: right;
    padding-right: 80px;
    padding-top: 45px;
}
.insight-countdown-product .item .product-price {
    position: absolute;
    z-index: 2;
    top: 50%;
    margin-top: -70px;
    border-radius: 100px;
    background-color: #fff;
    color: #000;
    text-align: center;
    padding: 5% 0px;
    height: 140px;
    width: 140px;
    font-size: 40px;
    font-weight: 900;
    font-style: italic;
    line-height: 35px;
}
.insight-countdown-product .special-bg {
    position: absolute;
    display: block;
    width: 100%;
    height: 100%;
    left: 100% !important;
    top: 0;
}
.insight-countdown-product .item .product-countdown {
    width: 90%;
    overflow: hidden;
    text-align: right;
    padding-right: 0px;
    padding-right: 80px;
}
.product-price.nd-font {
    border-radius: 100% !important;
    padding: 25px 0px !important;
    margin: 0px 50px;
    box-shadow: 10px 10px 0px 0px #34b2fb52;
}
.insight-countdown-product .item .product-price {
    position: absolute;
    z-index: 2;
    top: 50%;
    margin-top: -70px;
    border-radius: 100px;
    background-color: #fff;
    color: #000;
    text-align: center;
    padding: 5% 0px;
    height: 140px;
    width: 140px;
    font-size: 40px;
    font-weight: 900;
    font-style: italic;
    line-height: 35px;
    left: 25%;
}
a.carousel-control-prev {
    z-index: 0 !important;
    color: #000 !important;
    display: none;
    {{--  background: #0000000d;  --}}
    background: transparent;
    width: 35px !important;
}
a.carousel-control-next {
    z-index: 0 !important;
    color: #000 !important;
    display: none;
    {{--  background: #0000000d;  --}}
    background: transparent;
    width: 35px !important;
}
ol.carousel-indicators {
    background: transparent;
    width: auto !important;
    border-radius: 10px;
    color: #000 !important;
}
.insight-countdown-product .item .product-name {
    font-size: 23px;
    font-weight: 600;
    font-style: italic;
    color: #588288;
    line-height: 1;
    letter-spacing: 0.2em;
    margin-bottom: 32px;
}
.insight-countdown-product .item .product-image img {
    height: 645px !important;
    display: block;
    position: relative;
    z-index: 0 !important;
    max-height: 645px !important;
    {{--  box-shadow: 5px -5px 25px -5px black;  --}}
    {{--  border-right: 10px solid #b35958;  --}}
}
h2.title {
    width: 100%;
    overflow-wrap: break-word;
    padding: 50px 25px;
    margin-bottom: 10px !important;
    line-height: 50px !important;
    {{-- background-image: url(http://127.0.0.1:8000/image/bg-icons.png); --}}
    background-size: 25%;
    background-attachment: fixed;
    background-position: bottom;
    border-radius: 40%;
    padding-left: 0px;
    padding-right: 0px;
}
.insight-countdown-product .item .title {
    letter-spacing: 0.1em;
    text-transform: uppercase;
    line-height: 50px;
    font-family: -webkit-pictograph;
}
.product-desc {
    font-size: 16px;
}
.insight-countdown-product .item a {
    color: #fff;
    outline: none;
}
.woocommerce_loop_add_to_cart_span.hint--top.hint--rounded.hint--bounce.hint--success {
    background: #292929;
    padding: 5px 15px;
    border-radius: 5px;
}
.woocommerce_loop_add_to_cart_span.hint--top.hint--rounded.hint--bounce.hint--success:hover {
    background: #fbbf34;
    transition: 3s;
}
.hint--success.hint--top:before {
    border-top-color: #000;
}
.carousel-indicators li {
    box-sizing: content-box;
    flex: 0 1 auto;
    width: 30px;
    height: 3px;
    margin-right: 3px;
    margin-left: 3px;
    text-indent: -999px;
    cursor: pointer;
    background-color: #000;
    background-clip: padding-box;
    border-top: 10px solid transparent;
    border-bottom: 10px solid transparent;
    opacity: 0.5;
    transition: opacity .6s ease;
}
.product-countdown.col-md-6 {
    height: 535px;
    max-height: 535px;
}
.insight-countdown-product .item a {
    color: #333;
    outline: none;
}
img.keju {
    width: 15%;
    top: -85px;
    position: relative;
    left: -365px;
    display: inline-block;
    background: #fff;
    margin-bottom: -100px;
    border-radius: 100%;
    padding: 10px;
    box-shadow: 10px 10px #00000014;
}

@media (max-width: 768px) {
    div#home {
        margin-top: 85px;
        padding: 0px;
        background-size: 300%;
        background-repeat: no-repeat;
        background-position: left;
        background-position-x: 100%;
        background-position-y: 20px;
    }
    .insight-countdown-product .item .product-image img {
        height: 350px !important;
        display: block;
        position: relative;
        z-index: 0 !important;
        max-height: 645px !important;
    }
    h2.title {
        font-size: 29px;
        font-weight: 900;
        line-height: 37px !important;
        padding: 85px 20px;
        padding-left: 0px !important;
        margin-top: 40px;
        height: 255px;
        margin-left: 0px !important;
        font-family: sans-serif !important;
        background-size: 100%;
        background-attachment: fixed;
        background-position: bottom;
        border-radius: 0px !important;
        padding-left: 0px;
        margin-bottom: -45px !important;
    }
    .insight-countdown-product .item .product-name {
        text-align: right;
        font-size: 17px;
        font-weight: 600;
        font-style: italic;
        color: #588288;
        line-height: 1;
        letter-spacing: 0.2em;
        margin-bottom: 10px;
    }
    .product-price.nd-font {
        zoom: 75%;
        color: #191919 !important;
        background: #fff !important;
        top: 45% !important;
        border-radius: 101px !important;
        left: 0 !important;
        padding: 23px 0px !important;
        margin-left: 40px;
    }
    .product-countdown.col-md-6 {
        height: 515px;
        max-height: 515px;
    }
    .product-name.nd-font {
        margin-top: 15px;
        margin-bottom: 27px !important;
    }
    .product-price.nd-font {
        top: 23px !important;
    }
    .deal {
        display: none !important;
    }
    .product-price.nd-font {
        box-shadow: none;
        border: 5px solid grey;
        padding: 15px 0px !important;
    }
    .insight-countdown-product .item .product-countdown {
        padding-right: 0px !important;
        margin: 0px auto !important;
    }
    .insight-countdown-product .item .product-countdown {
        width: 90%;
        overflow: hidden;
        text-align: right;
        padding-right: 80px;
        padding-top: 0px;
    }
    .bungkus-kata {
        padding: 25px 20px;
    }
    img.keju {
        width: 20%;
        position: relative;
        top: -35px;
        left: -255px;
        display: inline-block;
        background: #fff;
    }
}
</style>

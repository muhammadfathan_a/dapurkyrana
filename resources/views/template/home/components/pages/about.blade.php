<div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid vc_custom_1478079338958" id="about">
    <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-offset-2 vc_col-lg-8 vc_col-md-offset-0 vc_col-md-12">
        <div class="vc_column-inner ">
            <div class="wpb_wrapper">
                <div class="insight-title text-center has-bg text-uppercase font-primary font-40 ofw-900 ofs-normal">
                    <h2 class="text-center"> Toko Kami </h2>
                <div class="sub-title primary-color"> - Toko Percaya - </div>
            </div>
            <div class="wpb_wrapper">
                {{-- <h2 class="text-center"> Nice Foody, Present! </h2> --}}
                <h2>
                    <img src="{{ URL::asset('image/logo.png') }}" class="about-img">
                </h2>
                <script id='custom-style-5d416d6c96c96' type='text/javascript'>
                    (function($) {$(document).ready(function() {$("head").append("<style>#heading-5d416d6c96c91{font-size: 18px;color: #333333;line-height: 2em;text-align: center;font-family:Playfair Display;font-weight:400;font-style:italic;text-transform: none;letter-spacing: .1em}</style>");});})(jQuery);
                </script>
                @foreach ($about as $ab)
                <div id="heading-5d416d6c96c91" class="vc_custom_heading mw-720 vc_custom_1563885786119">
                    {{-- <strong> Lorem Ipsum </strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. --}}
                    {{ $ab->description }}
                </div>
                @endforeach
            </div>
        </div>
    </div>
    <style>
    .insight-countdown-product .item {
        position: relative;
        z-index: 0 !important;
        outline: none;
    }
    img.about-img {
        width: 100%;
        padding: 0px 185px;
    }
    .insight-countdown-product .item .product-image img {
        max-width: 100%;
        height: auto;
        display: block;
        position: relative;
        z-index: 0 !important;
    }
    .img-product {
        z-index: 0 !important;
        display: flex;
    }
    .vc_custom_1563885786119 {
        padding-top: 0px !important;
        padding-bottom: 30px !important;
        background-image: url({{ URL::asset('image/bg-leafs.png') }});
        background-size: 50%;
        background-position: top;
        background-attachment: fixed;
        margin-top: 20px !important;
        border-radius: 10px;
    }
    div#about {
        margin: -145px 0px;
    }

@media (max-width: 768px) {
    div#about {
        margin: -50px 0px;
        margin-top: -135px;
        padding: 0px 35px;
    }
    h2.text-center {
        font-size: 27px;
    }
    img.about-img {
        width: 100%;
        padding: 0px;
        display: block;
        margin-bottom: -25px;
    }
    .vc_custom_1563885786119 {
        padding-top: 30px !important;
        padding-bottom: 30px !important;
        background-image: url({{ URL::asset('image/bg-leafs.png') }});
        background-size: 135%;
        background-position: top;
        background-attachment: fixed;
        margin-top: 20px !important;
        border-radius: 10px;
    }
    div#heading-5d416d6c96c91 {
        font-size: 17px;
    }
}
</style>

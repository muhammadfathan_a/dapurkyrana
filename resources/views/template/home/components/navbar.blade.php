{{--  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="/home"> Nice Foody </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse d-inline" id="navbarNav">
        <ul class="navbar-nav">
            <li class="nav-item active">
                <a class="nav-link" href="#home"> Beranda <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#about"> Tentang Kami </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#product"> Produk </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#blog"> Blog </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#contact"> Hubungi Kami </a>
            </li>
        </ul>
    </div>
</nav>  --}}
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    {{-- <a class="navbar-brand" href="#"> Foody Club </a> --}}
    <a class="navbar-brand" href="{{ URL::to('') }}">
        <img src="{{ URL::asset('image/logo.png') }}" width="130" alt="">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="#home"> Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#about"> Tentang Kami </a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Produk
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{ URL::to('?category=Coklat') }}">
                        Coklat
                    </a>
                    <a class="dropdown-item" href="{{ URL::to('?category=Frozen_Food') }}">
                        Makanan Beku
                    </a>
                    <a class="dropdown-item" href="{{ URL::to('?category=Keju') }}">
                        Keju
                    </a>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#blog"> Blog </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="https://instagram.com/dapurkyrana" target="_blank">
                    <ion-icon name="logo-instagram"></ion-icon>
                </a>
            </li>
            {{--  <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#"
                    id="navbarDropdown" role="button" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false"> Dropdown
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="#">Action</a>
                    <a class="dropdown-item" href="#">Another action</a>
                <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Something else here</a>
                </div>
            </li>  --}}
            {{--  <li class="nav-item">
                <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
            </li>  --}}
        </ul>
    </div>
</nav>
<div id="page" class="site">
<style>
html {
    scroll-behavior: smooth;
}
ion-icon.md.hydrated {
    font-size: 30px;
    zoom: 90%;
    color: #7db5bd;
}
@media (min-width: 1200px) {
    .container {
        max-width: 100% !important;
        padding: 0 !important;
        overflow: hidden;
    }
}
a:hover {
    text-decoration: none;
}
ul.navbar-nav {
    position: absolute;
    right: 5px;
}
li.nav-item {
    margin: 0px 15px;
}
.bg-light {
    position: fixed;
    z-index: 999 !important;
    width: 100%;
    height: 90px;
    background: #fff !important;
    box-shadow: 0px 0px 20px -10px #000 !important;
    border-bottom: 5px solid #fdbf34;
}
@media (max-width: 768px) {
    .bg-light {
        position: fixed;
        z-index: 999 !important;
        width: 100%;
        height: auto;
        box-shadow: 0px -5px 50px -25px #000;
        background: #fff !important;
        padding: 10px 15px;
        width: 100%;
    }
    .container {
        padding-left: 0px !important;
        padding-right: 0px !important;
    }
    ul.navbar-nav {
        position: relative !important;
        right: 0px !important;
    }
    li.nav-item {
        {{--  margin: 0px auto !important;  --}}
        margin: 0px !important;
    }
}
</style>

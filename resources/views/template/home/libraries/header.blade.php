<!DOCTYPE html>
<html lang="en-US">
    <head>

        <link rel="apple-touch-icon" sizes="57x57"          href="{{ URL::asset('favicon/apple-icon-57x57.png') }}">
        <link rel="apple-touch-icon" sizes="60x60"          href="{{ URL::asset('favicon/apple-icon-60x60.png') }}">
        <link rel="apple-touch-icon" sizes="72x72"          href="{{ URL::asset('favicon/apple-icon-72x72.png') }}">
        <link rel="apple-touch-icon" sizes="76x76"          href="{{ URL::asset('favicon/apple-icon-76x76.png') }}">
        <link rel="apple-touch-icon" sizes="114x114"        href="{{ URL::asset('favicon/apple-icon-114x114.png') }}">
        <link rel="apple-touch-icon" sizes="120x120"        href="{{ URL::asset('favicon/apple-icon-120x120.png') }}">
        <link rel="apple-touch-icon" sizes="144x144"        href="{{ URL::asset('favicon/apple-icon-144x144.png') }}">
        <link rel="apple-touch-icon" sizes="152x152"        href="{{ URL::asset('favicon/apple-icon-152x152.png') }}">
        <link rel="apple-touch-icon" sizes="180x180"        href="{{ URL::asset('favicon/apple-icon-180x180.png') }}">
        <link rel="icon" type="image/png" sizes="192x192"   href="{{ URL::asset('favicon/android-icon-192x192.png') }}">
        <link rel="icon" type="image/png" sizes="32x32"     href="{{ URL::asset('favicon/favicon-32x32.png') }}">
        <link rel="icon" type="image/png" sizes="96x96"     href="{{ URL::asset('favicon/favicon-96x96.png') }}">
        <link rel="icon" type="image/png" sizes="16x16"     href="{{ URL::asset('favicon/favicon-16x16.png') }}">
        <link rel="manifest" href="/manifest.json">
        <link rel="shortcut icon" href="{{ URL::asset('favicon/favicon.ico') }}" type="image/x-icon">

        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css">

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        {{--  <link href="https://unpkg.com/ionicons@4.2.2/dist/css/ionicons.min.css" rel="stylesheet">  --}}

            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1"/>

         {{-- <link rel="stylesheet" href="{{ URL::asset('css/home/font-awesome.min.css') }}" > --}}
            <link rel="profile" href="http://gmpg.org/xfn/11">

            {{-- get title --}}
            <title>
                {{-- @yield('title') --}}
                DapurKyrana | Welcome
            </title>

        <style id='rs-plugin-settings-inline-css' type='text/css'>
            #rs-demo-id {}
        </style>

        <style id='woocommerce-inline-inline-css' type='text/css'>
            .woocommerce form .form-row .required { visibility: visible; }
        </style>

        <link rel='stylesheet' id='wooscp-frontend-css'
            href="{{ URL::asset('css/home/frontend.css') }}"
            type='text/css' media='all'
        />
        <link rel='stylesheet' id='slick-css'  href='{{ URL::asset('css/home/slick.css') }}' type='text/css' media='all' />
        <link rel='stylesheet' id='main-style-css'  href='{{ URL::asset('css/home/style.css') }}' type='text/css' media='all' />
        <link rel='stylesheet' id='font-awesome-css'  href='{{ URL::asset('css/home/font-awesome.min.css') }}' type='text/css' media='all' />
        {{--  <link rel='stylesheet' id='ionicons-css'  href='{{ URL::asset('css/home/ionicons.css') }}' type='text/css' media='all' />  --}}
        <link rel='stylesheet' id='js_composer_front-css'  href='{{ URL::asset('css/home/js_composer.min.css') }}' type='text/css' media='all' />

        <link rel='stylesheet' id='kirki_google_fonts-css' href='https://fonts.googleapis.com/css?family=Lato%3A900%2C100italic%2C300italic%2Cregular%2Citalic%2C700italic%2C900italic%2C100italic%2C300italic%2Cregular%2Citalic%2C700italic%2C900italic%2C100italic%2C300italic%2Cregular%2Citalic%2C700italic%2C900italic%2C100italic%2C300italic%2Cregular%2Citalic%2C700italic%2C900italic%2C100italic%2C300italic%2Cregular%2Citalic%2C700italic%2C900italic%2C700%2C100italic%2C300italic%2Cregular%2Citalic%2C700italic%2C900italic%7CPlayfair+Display%3Aregular%2Cregular%2Citalic%2C700italic%2C900italic&#038;subset=latin-ext&#038;ver=3.0.15' type='text/css' media='all' />

            @yield('custom-css')
            <?php // include_once 'assets/custom/html/css.html'; ?>

        {{--  <script type='text/javascript' src='{{ URL::asset('js/home/jquery-3.4.1.min.js') }}'></script>  --}}
        {{--  <script type='text/javascript' src='{{ URL::asset('js/home/jquery-migrate.min.js') }}'></script>  --}}
        {{--  <script type='text/javascript' src='{{ URL::asset('js/home/jquery.themepunch.tools.min.js') }}'></script>  --}}
        {{--  <script type='text/javascript' src='{{ URL::asset('js/home/jquery.themepunch.revolution.min.js') }}'></script>  --}}
        {{--  <script type='text/javascript' src='{{ URL::asset('js/home/jquery.blockUI.min.js') }}'></script>  --}}
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

        <noscript>
            <style>.woocommerce-product-gallery{ opacity: 1 !important; }</style>
        </noscript>

        <meta name="generator" content="Powered by WPBakery Page Builder - drag and drop page builder for WordPress."/>
        <meta name="generator" content="Powered by Slider Revolution 5.4.7.3 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />

            @yield('custom-js')
            <?php // include_once 'assets/custom/html/js.html'; ?>

        <noscript>
            <style type="text/css">
                .wpb_animate_when_almost_visible { opacity: 1; }
            </style>
        </noscript>

        {{--  <script src="https://unpkg.com/ionicons@4.5.10-0/dist/ionicons.js"></script>  --}}

        <link rel="stylesheet" href="{{ URL::asset('css/home/header.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('css/home/corousel-slider.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('css/home/deal-of-the-day.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('css/home/our-product.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('css/home/our-blog.css') }}">
        {{--  <link rel="stylesheet" href="{{ URL::asset('css/home/food-credit.css') }}">  --}}

        {{--  <script type="module" src="https://unpkg.com/ionicons@4.5.10-0/dist/ionicons/ionicons.esm.js"></script>  --}}
        {{--  <script nomodule="" src="https://unpkg.com/ionicons@4.5.10-0/dist/ionicons/ionicons.js"></script>  --}}
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body class="home page-template-default page page-id-872 woocommerce-no-js page--fullwidth page-private--default tm-organie wpb-js-composer js-comp-ver-5.4.7 vc_responsive">

    @yield('navbar')
    @yield('content')
    @yield('footer')

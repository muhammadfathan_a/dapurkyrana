@extends('template/home/libraries/header')

    {{-- mendapatkan custom css nav --}}
    @section('custom-css')
        @include('template/home/customs/css')
    @endsection

    @section('title', 'Layanan | Foody')


    @section('navbar')
        @include('template/home/components/navbar')
    @endsection

    <div id="content">
        <div class="container content">
            @section('content')
            <div id="primary" class="content-area row">
                <div class="col-md-12">
                    <article id="post-872" class="post-872 page type-page status-publish hentry">
                        <div class="entry-content">
                            {{-- @include('template/home/components/pages/about') --}}
                        </div>
                        <div class="vc_row-full-width vc_clearfix"></div>
                            {{-- @include('template/home/components/pages/deal-of-the-day') --}}
                        <div class="vc_row-full-width vc_clearfix"></div>
                            {{-- @include('template/home/components/pages/our-product') --}}
                        {{-- <div class="vc_row-full-width vc_clearfix"></div>
                            @include('template/home/components/pages/testimony') --}}
                        <div class="vc_row-full-width vc_clearfix"></div>
                            {{-- @include('template/home/components/pages/our-blog') --}}
                    </article>
                </div>
            </div>
        </div>
        @endsection
    @section('footer')
        @include('template/home/components/footer')
    @endsection

    @section('custom-js')
        @include('template/home/customs/js')
    @endsection

@extends('template/home/libraries/footer')

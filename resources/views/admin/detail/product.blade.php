@include('template/admin/libraries/header')
@extends('template/admin/app')

@section('content')
<div class="container-fluid">
    <div class="row">
        @include('template/admin/components/sidebar')
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2"> Detail Produk <br> <small style="font-size: 16px; margin: 10px 0px;"> Mananajemen catalog produk anda </small> </h1>
            </div>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ URL::to('admin') }}"> Home </a></li>
                <li class="breadcrumb-item"><a href="{{ URL::to('admin/product') }}"> Product </a></li>
                <li class="breadcrumb-item active" aria-current="page"> Detail </li>
                </ol>
            </nav>
            <div style="overflow-x:auto;">
            @foreach ($product as $prod)
                @foreach ($image as $img)
                    @if ($prod->id_image == $img->id_image)
                        <div class="card" style="width: 18rem;">
                            <img src="{{ URL::asset($img->nm_image) }}" class="card-img-top">
                        <div class="card-body">
                            <h5 class="card-title">
                                <b>{{ $prod->nm_product }}</b>
                                <br>
                                <br>
                                @foreach ($category as $cat)
                                    @if ($prod->id_category == $cat->id_category)
                                        Kategori : <small>
                                            <b> {{ $cat->nm_category }} </b>
                                        </small>
                                    @else
                                        Kategori : <small>
                                            <b> Ubah untuk memberikan kategori </b>
                                        </small>
                                    @endif
                                @endforeach
                                <br>
                                <br>
                                @foreach ($segment as $seg)
                                    @if ($prod->id_segment == $seg->id_segment)
                                        Segmen : <small>
                                            <b> {{ $seg->nm_segment }} </b>
                                        </small>
                                    @else
                                        Segmen : <small>
                                            <b> Ubah untuk memberikan segmen </b>
                                        </small>
                                    @endif
                                @endforeach
                            </h5>
                            <hr>
                            <p class="card-text">
                                Deskripsi : <b> "{{ $prod->description }}" </b>
                            </p>
                            <a href="{{ URL::to('admin/product/edit/'.$prod->id_product) }}"
                                class="badge badge-success p-2 m-1"> Edit
                            </a>
                            |
                            <a href="{{ URL::to('admin/product/delete/'.$prod->id_product) }}"
                                class="badge badge-danger p-2 m-1"> Hapus
                            </a>
                        </div>
                    </div>
                    @endif
                @endforeach
            @endforeach
            </div>
            <hr>
        </main>
    </div>
</div>
@endsection
@include('template/admin/libraries/footer')
<style>
textarea.form-control {
    height: 125px !important;
}
table.table.catalog {
    width: max-content;
    overflow-x: auto;
}
.card {
    width: 100% !important;
    margin: 0 auto !important;
    padding: 0 !important;
    border: none !important;
}
img.card-img-top {
    width: 45%;
}

@media (max-width: 768px) {
    img.card-img-top {
        width: 100%;
    }
}
</style>

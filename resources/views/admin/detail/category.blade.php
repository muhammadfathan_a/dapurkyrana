@include('template/admin/libraries/header')
@extends('template/admin/app')

@section('content')
<div class="container-fluid">
    <div class="row">
        @include('template/admin/components/sidebar')
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            @foreach ($nm as $nmCategory)
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">Detail Kategori:
                    <small>
                        <b>{{ $nmCategory->nm_category }}</b>
                    </small>
                    <br>
                    <small style="font-size: 16px; line-height: 50px;">
                        Mananajemen kategori produk anda
                    </small>
                </h1>
                <div class="col-sm-8 text-right">
                    <button class="btn btn-success d-inline" data-toggle="modal" data-target="#modal-produk">
                        Ubah Kategori
                    </button>
                    <form action="/admin/category/delete/{{ $nmCategory->id_category }}" method="post" class="d-inline">
                        @method('delete')
                        @csrf
                        <button class="btn btn-danger">
                            Hapus Kategori
                        </button>
                    </form>
                    <p class="mt-1"> Cek diproduk tak berkategori </p>
                </div>
            </div>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ URL::to('admin') }}"> Home </a></li>
                <li class="breadcrumb-item"><a href="{{ URL::to('admin/category') }}"> Category </a></li>
                <li class="breadcrumb-item active" aria-current="page"> Detail </li>
                </ol>
            </nav>
            @endforeach
            <div class="row">
                @foreach ($product as $item)
                <div class="col-sm-4">
                    <div class="card">
                        @foreach ($image as $img)
                            @if ($item->id_image == $img->id_image)
                            <a href="{{ URL::to('admin/product/detail/'.$item->id_product) }}">
                                <img src="{{ URL::asset($img->nm_image) }}" class="card-img-top">
                            </a>
                            <small> Klik gambar untuk megubah produk </small>
                            @endif
                        @endforeach
                        <div class="card-body">
                            <h5 class="card-title">
                                <b>{{ $item->nm_product }}</b>
                            <br>
                            </h5>
                            <p class="card-text">
                                Deskripsi : <b>"{{ $item->description }}"</b>
                            </p>
                            <p class="card-text" style="border-top: 1px solid silver; padding: 10px 0px;">
                                Tanggal Post : <b>{{ $item->created_at }}</b>
                            </p>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            {{-- @foreach ($product as $prod)
                @foreach ($category as $cat)
                    @foreach ($image as $img)
                        @if ($prod->id_image == $img->id_image)
                            @if ($prod->id_category == $cat->id_category)
                            <div class="card" style="width: 18rem;">
                                <img src="{{ URL::asset($img->nm_image) }}" class="card-img-top">
                                <div class="card-body">
                                    <h5 class="card-title">{{ $prod->nm_product }}
                                        <br>
                                        <small> {{ $cat->nm_category }} </small>
                                    </h5>
                                    <p class="card-text">{{ $prod->description }}</p>
                                    <a href="/admin/product/edit/{{ $prod->id_product }}"
                                        class="badge badge-success p-2 m-1"> Edit
                                    </a>
                                    |
                                    <a href="/admin/product/delete/{{ $prod->id_product }}"
                                        class="badge badge-danger p-2 m-1"> Hapus
                                    </a>
                                </div>
                            </div>
                            @endif
                        @endif
                    @endforeach
                @endforeach
            @endforeach --}}
            </div>
            <hr>

            @foreach ($nm as $nmCategory)
            <!-- Modal -->
            <div class="modal fade" id="modal-produk" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel"> Tambah Kategori </h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form method="post" action="/admin/category/update/{{ $nmCategory->id_category }}"
                            enctype="multipart/form-data">
                            @csrf
                            <div class="modal-body">
                                <div class="form-row">
                                    <div class="col-md-12 mb-3" style="display: none;">
                                        <label for="validationServer01"> ID Kategori </label>
                                        <input type="text" class="form-control" id="validationServer01" placeholder="Nama Kategori" required name="id_kategori"
                                        value="{{ $nmCategory->id_category }}">
                                    </div>
                                    <div class="col-md-12 mb-3">
                                        <label for="validationServer01"> Nama Kategori </label>
                                        <input type="text" class="form-control" id="validationServer01" placeholder="Nama Kategori" required name="nm_kategori"
                                        value="{{ $nmCategory->nm_category }}">
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal"> Batalkan </button>
                                <button class="btn btn-primary" type="submit"> Simpan Perubahan </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            @endforeach
        </main>
    </div>
</div>
@endsection
@include('template/admin/libraries/footer')
<style>
textarea.form-control {
    height: 125px !important;
}
table.table.catalog {
    width: max-content;
    overflow-x: auto;
}
.card {
    width: 100% !important;
    margin: 0 auto !important;
    padding: 0 !important;
    border: none !important;
}
img.card-img-top {
    width: 100%;
}
.card {
    box-shadow: 0px 0px 15px -3px silver;
    padding: 25px !important;
    max-height: 435px !important;
    height: 435px !important;
    margin: 30px 0px !important;
}
img.card-img-top {
    height: 235px;
    max-height: 235px;
}
img.card-img-top:hover {
    /* transform: rotate3d(0, 15, 0, 360deg); */
    opacity: 0.6;
    transition: 1s;
    cursor: pointer;
    border-radius: 10px;
}

@media (max-width: 768px) {
    .card {
        margin: 0 !important;
        margin-bottom: 25px !important;
    }
}
</style>

@include('template/admin/libraries/header')
@extends('template/admin/app')

@section('content')
<div class="container-fluid">
    <div class="row">
        @include('template/admin/components/sidebar')
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2"> Produk Berkategori
                    <br>
                    <small style="font-size: 16px; margin: 10px 0px;">
                        Mananajemen produk anda
                    </small>
                </h1>
                <div class="col-sm-5 btn-product">
                    <a href="{{ URL::to('admin/product/withoutcategory') }}" class="btn-to-other">
                        <button class="btn btn-primary" style="color: #fff;">
                            Tak Berkategori
                        </button>
                    </a>
                    <a href="{{ URL::to('admin/product/withoutsegment') }}'" class="btn-to-other">
                        <button class="btn btn-danger" style="color: #fff;">
                            Tak Bersegmentasi
                        </button>
                    </a>
                    <span class="p-1"> | </span>
                    <button class="btn btn-success" data-toggle="modal" data-target="#modal-product">
                        Tambahkan Produk
                    </button>
                </div>
            </div>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ URL::to('admin') }}"> Home </a></li>
                <li class="breadcrumb-item active" aria-current="page"> Product </li>
                </ol>
            </nav>
            <input id="myInput" class="form-control form-control-sm-12 mb-3" type="text" placeholder="Cari nama produk" onkeyup="myFunction()">
            <div style="overflow-x:auto;">
                <table class="table catalog" id="myTable">
                    <thead class="thead-dark">
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col" style="text-align:left;"> Opsi </th>
                        <th scope="col"> Nama Produk </th>
                        <th scope="col"> Kategori </th>
                        <th scope="col"> Segmentasi </th>
                        <th scope="col"> Harga </th>
                        <th scope="col"> Tanggal Post </th>
                        </tr>
                    </thead>
                    <?php $no = 1; ?>
                    @foreach ($product as $prod)
                        @foreach ($category as $cat)
                            @foreach ($segment as $seg)
                                @if ($prod->id_segment == $seg->id_segment)
                                    @if ($prod->id_category == $cat->id_category)
                                    <tbody class="tbody catalog">
                                        <tr>
                                            <th scope="row">{{ $no++ }}</th>
                                            <td>
                                                <a href="{{ URL::to('admin/product/detail/'.$prod->id_product) }}"
                                                    class="badge badge-warning p-2 m-1"
                                                    style="color: #000;">
                                                    Lihat Detail
                                                </a>
                                            </td>
                                            <td>{{ $prod->nm_product }}</td>
                                            <td>
                                                <a href="{{ URL::to('admin/category/detail/'.$cat->id_category) }}">
                                                    {{ $cat->nm_category }}
                                                </a>
                                            </td>
                                            <td>
                                                <a href="{{ URL::to('admin/segment/detail/'.$seg->id_segment) }}">
                                                    {{ $seg->nm_segment }}
                                                </a>
                                            </td>
                                            <td>Rp{{ number_format($prod->price,0) }}</td>
                                            <td>{{ $prod->created_at }}</td>
                                        <tr>
                                    </tbody>
                                    @endif
                                @endif
                            @endforeach
                        @endforeach
                    @endforeach
                </table>
            </div>
            {{--  {{ $product->links() }}  --}}

            <!-- Modal -->
            <div class="modal fade" id="modal-product" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel"> Tambah Produk </h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form method="post" action="{{ URL::to('admin/product') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="modal-body">
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label for="validationServer01"> Nama Produk </label>
                                        <input type="text" class="form-control" id="validationServer01" placeholder="Nama Produk" required name="nm_produk">
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <label for="validationServer01"> Kategori </label>
                                        <select name="id_category" id="category" class="form-control" id="validationServer01" >
                                            <option value="null"> -- Pilih Kategori -- </option>
                                            @foreach ($category as $item)
                                                <option value="{{ $item->id_category }}">{{ $item->nm_category }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label for="validationServer01"> Deskripsi </label>
                                        <textarea class="form-control" id="placeHolder" placeholder="Tulis Deskripsi Produk" required name="desc_produk"></textarea>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <label for="validationServer01"> Segment </label>
                                        <select name="id_segment" id="category" class="form-control" id="validationServer01">
                                            <option value="null"> -- Pilih Segment -- </option>
                                            @foreach ($segment as $item)
                                                <option value="{{ $item->id_segment }}">
                                                    {{ $item->nm_segment }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label for="validationServer01"> Harga </label>
                                        <input type="number" class="form-control" id="validationServer01" placeholder="Harga Produk" required name="harga_produk">
                                        <small>Perhatikan jumlah angka nol nya</small>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <label for="validationServer01"> Unggah Foto </label>
                                        <input type="file" class="form-control" id="validationServer01" placeholder="Gambar Produk" required name="img_produk">
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal"> Batalkan </button>
                                <button class="btn btn-primary" type="submit"> Tambahkan </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </main>
    </div>
</div>
@endsection
@include('template/admin/libraries/footer')
<style>
textarea.form-control {
    height: 125px !important;
}
table.table.catalog {
    width: 100%;
    overflow-x: auto;
}
.col-sm-5.btn-product {
    text-align: right;
}
.active-pink-2 input[type=text]:focus:not([readonly]) {
    border-bottom: 1px solid #f48fb1;
    box-shadow: 0 1px 0 0 #f48fb1;
}

@media (max-width: 768px) {
    .col-sm-5.btn-product {
        display: inline-flex;
        text-align: center;
        width: 100%;
        padding: 0;
        margin: 0 auto;
        margin-top: 35px;
    }
    a.btn-to-other {
        padding-right: 10px;
    }
    table#myTable {
        width: max-content;
    }
}
</style>

<script>
function myFunction() {
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    table = document.getElementById("myTable");
    tr = table.getElementsByTagName("tr");
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[1];
        if (td) {
            txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}
</script>

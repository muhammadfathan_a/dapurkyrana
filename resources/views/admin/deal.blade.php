@include('template/admin/libraries/header')
@extends('template/admin/app')

@section('content')
<div class="container-fluid">
    <div class="row">
        @include('template/admin/components/sidebar')
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2"> Promosi Produk
                    <br>
                    <small style="font-size: 16px; margin: 10px 0px;">
                        Mananajemen promosi produk anda
                    </small>
                </h1>
                <div class="col-sm-5 btn-product">
                    <button class="btn btn-success" data-toggle="modal" data-target="#modal-deal">
                        Buat Promosi Produk
                    </button>
                </div>
            </div>
            <nav aria-label="breadcrumb" class="breadcrumb-custom">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ URL::to('admin') }}"> Home </a></li>
                <li class="breadcrumb-item active" aria-current="page"> Deal </li>
                </ol>
            </nav>
            <input id="myInput" class="form-control form-control-sm-12 mb-3" type="text" placeholder="Cari judul promosi" onkeyup="myFunction()">
            <div style="overflow-x:auto;">
                <table class="table catalog" id="myTable">
                    <thead class="thead-dark">
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col" style="text-align:left;"> Opsi </th>
                        <th scope="col"> Judul Promosi </th>
                        <th scope="col"> Sub Judul Promosi </th>
                        <th scope="col"> Harga Promosi </th>
                        <th scope="col"> Tanggal Post </th>
                        </tr>
                    </thead>
                    <?php $no = 1; ?>
                    @foreach ($deal as $deals)
                        <tbody class="tbody catalog">
                            <tr>
                                <th scope="row">{{ $no++ }}</th>
                                <td>
                                    <a href="{{ URL::to('admin/deal/detail/'.$deals->id_deal) }}"
                                        class="badge badge-warning p-2 m-1"
                                        style="color: #000;">
                                        Lihat Detail
                                    </a>
                                </td>
                                <td>{{ $deals->title_deal }}</td>
                                <td>
                                    {{ $deals->subtitle_deal }}
                                </td>
                                <td>Rp{{ number_format( $deals->price,0) }}</td>
                                <td>{{ $deals->created_at }}</td>
                            <tr>
                        </tbody>
                    @endforeach
                </table>
            </div>
            {{ $deal->links() }}

            <!-- Modal -->
            <div class="modal fade" id="modal-deal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel"> Buat Promosi </h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form method="post" action="/admin/deal" enctype="multipart/form-data">
                            @csrf
                            <div class="modal-body">
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label for="validationServer01"> Judul Promosi </label>
                                        <input type="text" class="form-control" id="validationServer01" placeholder="Judul Promosi" required name="title_deal">
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <label for="validationServer01"> Sub Judul Promosi </label>
                                        <input type="text" class="form-control" id="validationServer01" placeholder="Sub Judul Promosi" required name="subtitle_deal">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label for="validationServer01"> Deskripsi </label>
                                        <textarea class="form-control" id="validationServer01" placeholder="Tulis Deskripsi Produk" required name="desc_deal"></textarea>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <label for="validationServer01"> Kategori </label>
                                        <select name="id_category" id="category" class="form-control" id="validationServer01" >
                                            <option value="null"> -- Pilih Kategori -- </option>
                                            @foreach ($category as $item)
                                                <option value="{{ $item->id_category }}">{{ $item->nm_category }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label for="validationServer01"> Harga </label>
                                        <input type="number" class="form-control" id="validationServer01" placeholder="Harga Produk" required name="harga_deal">
                                        <small>Perhatikan jumlah angka nol nya</small>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <label for="validationServer01"> Unggah Foto </label>
                                        <input type="file" class="form-control" id="validationServer01" placeholder="Gambar Produk" required name="img_deal">
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal"> Batalkan </button>
                                <button class="btn btn-primary" type="submit"> Buatkan </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </main>
    </div>
</div>
@endsection
@include('template/admin/libraries/footer')
<style>
textarea.form-control {
    height: 125px !important;
}
table.table.catalog {
    width: 100%;
    overflow-x: auto;
}
.col-sm-5.btn-product {
    text-align: right;
}

@media (max-width: 768px) {
    .col-sm-5.btn-product {
        text-align: left;
        padding: 0 !important;
        margin-top: 30px !important;
    }
    nav.breadcrumb-custom {
        width: 100% !important;
        padding: 0px !important;
    }
    table#myTable {
        width: max-content;
    }
}
</style>

<script>
function myFunction() {
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    table = document.getElementById("myTable");
    tr = table.getElementsByTagName("tr");
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[1];
        if (td) {
            txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}
</script>
        
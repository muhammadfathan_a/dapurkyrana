@include('template/admin/libraries/header')
@extends('template/admin/app')

@section('content')
<div class="container-fluid">
    <div class="row">
        @include('template/admin/components/sidebar')
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2"> Catalog <br>
                    <small style="font-size: 16px; margin: 10px 0px;">
                        Mananajemen catalog produk anda
                    </small>
                </h1>
            </div>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ URL::to('admin') }}"> Home </a></li>
                <li class="breadcrumb-item"><a href="{{ URL::to('admin/product') }}"> Product </a></li>
                <li class="breadcrumb-item active" aria-current="page"> Edit </li>
                </ol>
            </nav>
            <div style="overflow-x:auto;">
                <!-- Modal -->
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"> Edit Produk </h5>
                    </div>
                    <form method="post" action="{{ URL::to('admin/product/update/'.$product->id_product) }}" enctype="multipart/form-data">
                        @csrf
                        <div class="modal-body">
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationServer01"> Nama Produk </label>
                                    <input type="text" class="form-control" id="validationServer01" placeholder="Nama Produk" required name="nm_produk"
                                    value="{{ $product->nm_product }}">
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="validationServer01"> Kategori </label>
                                    <select name="id_category" id="category" class="form-control" id="validationServer01">
                                        <option value="null"> -- Pilih Kategori -- </option>
                                        @foreach ($category as $item)
                                            <option value="{{ $item->id_category }}">
                                                {{ $item->nm_category }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationServer01"> Deskripsi </label>
                                    <textarea class="form-control" id="validationServer01" placeholder="Tulis Deskripsi Produk"
                                    required name="desc_produk">{{$product->description}}</textarea>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="validationServer01"> Segment </label>
                                    <select name="id_segment" id="category" class="form-control" id="validationServer01">
                                        <option value="null"> -- Pilih Segment -- </option>
                                        @foreach ($segment as $item)
                                            <option value="{{ $item->id_segment }}">
                                                {{ $item->nm_segment }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationServer01"> Harga </label>
                                    <input type="number" class="form-control" id="validationServer01" placeholder="Harga Produk" required name="harga_produk"
                                    value="{{ $product->price }}">
                                    <small>Perhatikan jumlah angka nol nya</small>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="validationServer01"> Unggah Foto </label>
                                    <input type="file" class="form-control" id="validationServer01" placeholder="Gambar Produk" required name="img_produk">
                                    <small> Masukkan foto kembali </small>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-primary" type="submit" style="width: 100%;">
                                Ubah
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </main>
    </div>
</div>
@endsection
@include('template/admin/libraries/footer')
<style>
textarea.form-control {
    height: 125px !important;
}
table.table.catalog {
    width: max-content;
    overflow-x: auto;
}
</style>

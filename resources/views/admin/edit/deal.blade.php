@include('template/admin/libraries/header')
@extends('template/admin/app')

@section('content')
<div class="container-fluid">
    <div class="row">
        @include('template/admin/components/sidebar')
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2"> Promosi <br>
                    <small style="font-size: 16px; margin: 10px 0px;">
                        Mananajemen promosi produk anda
                    </small>
                </h1>
            </div>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ URL::to('admin') }}"> Home </a></li>
                <li class="breadcrumb-item"><a href="{{ URL::to('admin/deal') }}"> Deal </a></li>
                <li class="breadcrumb-item active" aria-current="page"> Edit </li>
                </ol>
            </nav>
            <div style="overflow-x:auto;">
                <!-- Modal -->
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"> Edit Promosi </h5>
                    </div>
                    <form method="post" action="{{ URL::to('admin/deal/update/'.$deal->id_deal) }}" enctype="multipart/form-data">
                        @csrf
                        <div class="modal-body">
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationServer01"> Judul Promosi </label>
                                    <input type="text" class="form-control" id="validationServer01" placeholder="Judul Promosi" required name="title_deal" value="{{ $deal->title_deal }}">
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="validationServer01"> Sub Judul Promosi </label>
                                    <input type="text" class="form-control" id="validationServer01" placeholder="Sub Judul Promosi" required name="subtitle_deal" value="{{ $deal->subtitle_deal }}">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationServer01"> Deskripsi </label>
                                    <textarea class="form-control" id="validationServer01" placeholder="Tulis Deskripsi Produk" required name="desc_deal"> {{ $deal->description }} </textarea>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="validationServer01"> Kategori </label>
                                    <select name="id_category" id="category" class="form-control" id="validationServer01" >
                                        <option value="null"> -- Pilih Kategori -- </option>
                                        @foreach ($category as $item)
                                            <option value="{{ $item->id_category }}">{{ $item->nm_category }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationServer01"> Harga </label>
                                    <input type="number" class="form-control" id="validationServer01" placeholder="Harga Produk" required name="harga_deal" value="{{ $deal->price }}">
                                    <small>Perhatikan jumlah angka nol nya</small>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="validationServer01"> Unggah Foto </label>
                                    <input type="file" class="form-control" id="validationServer01" placeholder="Gambar Produk" required name="img_deal">
                                    <small> Nama gambar : "{{ $image->nm_image }}" </small>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-primary" type="submit"> Ubah </button>
                        </div>
                    </div>
                </form>
            </div>
        </main>
    </div>
</div>
@endsection
@include('template/admin/libraries/footer')
<style>
textarea.form-control {
    height: 125px !important;
}
table.table.catalog {
    width: max-content;
    overflow-x: auto;
}
</style>

@include('template/admin/libraries/header')
@extends('template/admin/app')

@section('content')
<div class="container-fluid">
    <div class="row">
        @include('template/admin/components/sidebar')
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2"> Tentang Kami </h1>
                <span> Deskripsi tentang kami hanya 1 data </span>
            </div>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ URL::to('admin') }}"> Home </a></li>
                <li class="breadcrumb-item active" aria-current="page"> About Us </li>
                </ol>
            </nav>
            <input id="myInput" class="form-control form-control-sm-12 mb-3" type="text" placeholder="Cari nama kategori" onkeyup="myFunction()">
            <div style="overflow-x:auto;">
                <table class="table kategori" id="myTable">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col" class="th option"> Opsi </th>
                            <th scope="col"> Description </th>
                            <th scope="col" class="text-center"> Tanggal Post </th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($about as $ab)
                        <tr>
                            <th scope="row"> X </th>
                            <td class="option">
                                <button class="btn btn-info btn-prod text-light" data-toggle="modal" data-target="#modal-produk">
                                    Edit
                                </button>
                            </td>
                            <td class="td desc"> {{ $ab->description }} </td>
                            <td class="text-center"> {{ $ab->created_at }} </td>
                        <tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <hr>

            @foreach ($about as $ab)
            <!-- Modal -->
            <div class="modal fade" id="modal-produk" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel"> Mengubah Deskripsi </h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form method="post" action="{{ URL::to('admin/about-us/update/{about}') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="modal-body">
                                <div class="form-row">
                                    <div class="col-md-12 mb-3">
                                        <label for="validationServer01"> Deskripsi </label>
                                        {{-- <input type="text" class="form-control" id="validationServer01" placeholder="Tulis Deskripsi" required name="nm_kategori"> --}}
                                        <textarea class="form-control" id="validationServer01" placeholder="Tulis Deskripsi" required name="desc_about">{{$ab->description}}</textarea>
                                        <input type="text" name="id_about" value="{{ $ab->id_about }}" style="display: none;">
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal"> Batalkan </button>
                                <button class="btn btn-primary" type="submit"> Ubah </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            @endforeach
        </main>
    </div>
</div>
@endsection
<style>
td.td.desc {
    width: 50%;
}
</style>
@include('template/admin/libraries/footer')

@include('template/admin/libraries/header')
@extends('template/admin/app')

@section('content')
<div class="container-fluid">
    <div class="row">
        @include('template/admin/components/sidebar')
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2"> Produk Tak Bersegmentasi
                    <br>
                    <small style="font-size: 16px; margin: 10px 0px;">
                        Mananajemen produk anda
                    </small>
                </h1>
                <div class="col-sm-5 btn-product">
                    <a href="{{ URL::to('admin/product') }}">
                        <button class="btn btn-primary" style="color: #fff;">
                            Lihat Produk
                        </button>
                    </a>
                </div>
            </div>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ URL::to('admin') }}"> Home </a></li>
                <li class="breadcrumb-item"><a href="{{ URL::to('admin/product') }}"> Product </a></li>
                <li class="breadcrumb-item active" aria-current="page"> Other </li>
                </ol>
            </nav>
            <input id="myInput" class="form-control form-control-sm-12 mb-3" type="text" placeholder="Cari nama produk" onkeyup="myFunction()">
            <div style="overflow-x:auto;">
                <table class="table catalog" id="myTable">
                    <thead class="thead-dark">
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col" style="text-align:left;"> Opsi </th>
                        <th scope="col"> Nama Produk </th>
                        <th scope="col"> Kategori </th>
                        <th scope="col"> Deskripsi </th>
                        <th scope="col"> Harga </th>
                        <th scope="col"> Tanggal Post </th>
                        </tr>
                    </thead>
                    <?php $no = 1; ?>
                    @foreach ($product as $prod)
                    <tbody class="tbody catalog">
                        <tr>
                            <th scope="row">{{ $no++ }}</th>
                            <td>
                                <a href="{{ URL::to('admin/product/detail/'.$prod->id_product) }}"
                                    class="badge badge-warning p-2 m-1"
                                    style="color: #000;">
                                    Lihat Detail
                                </a>
                            </td>
                            <td>{{ $prod->nm_product }}</td>
                            <td> - </td>
                            <td>{{ $prod->description }}</td>
                            <td>Rp{{ number_format($prod->price,0) }}</td>
                            <td>{{ $prod->created_at }}</td>
                        <tr>
                    </tbody>
                    @endforeach
                </table>
            </div>
            {{ $product->links() }}
        </main>
    </div>
</div>
@endsection
@include('template/admin/libraries/footer')
<style>
textarea.form-control {
    height: 125px !important;
}
table.table.catalog {
    width: 100%;
    overflow-x: auto;
}
.col-sm-5.btn-product {
    text-align: right;
}

@media (max-width: 768px) {
    .col-sm-5.btn-product {
        display: inline-flex;
        text-align: center;
        width: 100%;
        padding: 0;
        margin: 0 auto;
        margin-top: 35px;
    }
    a.btn-to-other {
        padding-right: 10px;
    }
    table#myTable {
        width: max-content;
    }
}
</style>

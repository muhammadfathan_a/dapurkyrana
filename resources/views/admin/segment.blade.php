@include('template/admin/libraries/header')
@extends('template/admin/app')

@section('content')
<div class="container-fluid">
    <div class="row">
        @include('template/admin/components/sidebar')
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2"> Segment
                    <br>
                    <small style="font-size: 16px; margin: 10px 0px;">
                        Mananajemen segmen produk anda
                    </small>
                </h1>
                <button class="btn btn-success btn-prod" data-toggle="modal" data-target="#modal-produk">
                    Tambahkan Kategori Baru
                </button>
            </div>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ URL::to('admin') }}"> Home </a></li>
                    <li class="breadcrumb-item active" aria-current="page"> Segment </li>
                </ol>
            </nav>
            <input id="myInput" class="form-control form-control-sm-12 mb-3" type="text" placeholder="Cari nama kategori" onkeyup="myFunction()">
            <div style="overflow-x:auto;">
                <table class="table kategori" id="myTable">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col"> Nama Kategori </th>
                            <th scope="col"> Tanggal Post </th>
                            <th scope="col" class="th option"> Opsi </th>
                        </tr>
                    </thead>
                    <?php $no = 1; ?>
                    @foreach ($segment as $seg)
                        <tbody>
                            <tr>
                                <th scope="row">{{ $no++ }}</th>
                                <td>{{ $seg->nm_segment }}</td>
                                <td>{{ $seg->created_at }}</td>
                                <td class="option">
                                    <a href="{{ URL::to('admin/segment/detail/'.$seg->id_segment) }}"
                                        class="badge badge-info p-2 m-1"
                                        style="color: #fff;">
                                        Lihat Produk
                                    </a>
                                </td>
                            <tr>
                        </tbody>
                    @endforeach
                </table>
            </div>
            <hr>

            <!-- Modal -->
            <div class="modal fade" id="modal-produk" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel"> Tambah Segmen </h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form method="post" action="{{ URL::to('admin/segment') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="modal-body">
                                <div class="form-row">
                                    <div class="col-md-12 mb-3">
                                        <label for="validationServer01"> Nama Segmen </label>
                                        <input type="text" class="form-control" id="validationServer01" placeholder="Nama Segmen" required name="nm_segment">
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal"> Batalkan </button>
                                <button class="btn btn-primary" type="submit"> Tambahkan </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </main>
    </div>
</div>
@endsection
@include('template/admin/libraries/footer')
<style>
table.table.kategori {
    width: 100%;
    text-align: center;
}
td.option {
    text-align: right;
}
th.th.option {
    text-align: right;
    padding-right: 50px;
}

@media (max-width: 768px) {
    #myTable {
        width: max-content;
    }
    button.btn.btn-success.btn-prod {
        margin-top: 25px;
    }
}
</style>
<script>
function myFunction() {
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    table = document.getElementById("myTable");
    tr = table.getElementsByTagName("tr");
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[0];
        if (td) {
            txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}
</script>
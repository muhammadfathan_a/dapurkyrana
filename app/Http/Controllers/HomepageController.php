<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Vinkla\Instagram\Instagram;

use DB;
use App\Deal;
use App\Product;
use App\Category;
use App\Image;
use App\About;
use App\Segment;

class HomepageController extends Controller
{

    public function home()
    {
        error_reporting(1);
     
        $ClientID       = 'a31e7f21657e48e68c94e4cb34264f1e';
        $ClientSecret   = '4a83315fcf374b7eb4c1174c114cf950';
        $urlAPI         = 'https://api.instagram.com/v1/users/self/media/recent/?access_token=18482822258.1677ed0.5be30c9bd221434d87ed657560642561&count=6';
        
        $curl = curl_init();// set url
		
        curl_setopt($curl, CURLOPT_URL, $urlAPI);
        
		curl_setopt( $curl, CURLOPT_RETURNTRANSFER, 1);

		$result = curl_exec($curl);
		curl_close($curl);
		
        $result = json_decode($result, true);
        
        foreach($result['data'] as $photo) {
            $photos[] = $photo['images']['standard_resolution']['url'];
        }

        $product    = DB::table('product')->orderBy('created_at', 'DESC')->paginate(100);
        $category   = Category::all();
        $image      = Image::all();
        $deal       = Deal::all();
        $about      = About::all();
        $segment    = Segment::all();

        return view('home/index', [
            'deal'      => $deal,
            'product'   => $product,
            'category'  => $category,
            'segment'   => $segment,
            'image'     => $image,
            'about'     => $about,
            'insta_pic' => $photos
        ]);
    }

    public function product()
    {
        // $product    = DB::table('product')->chunk(100)->orderBy('created_at', 'DESC')->paginate(8);

        $product    = DB::table('product')->orderBy('created_at', 'DESC')->paginate(100);
        $category   = Category::all();
        $image      = Image::all();
        return view('home/product', [
            'product'   => $product,
            'category'  => $category,
            'image'     => $image
        ]);
    }

    public function gallery()
    {
        // // $product    = DB::table('product')->orderBy('created_at', 'DESC')->paginate(8);

        // $product    = DB::table('product')->orderBy('created_at', 'DESC')->paginate(100);
        // $category   = Category::all();
        // $image      = Image::all()->first();
        // $deal       = Deal::all();

        // return view('home/gallery', [
        //     'deal'      => $deal,
        //     'product'   => $product,
        //     'category'  => $category,
        //     'image'     => $image
        // ]);
    }

    public function service()
    {
        return view('home/service');
    }

    public function blog()
    {
        return view('home/blog');
    }

    public function about()
    {
        return view('home/about');
    }

    public function contact_us()
    {
        return view('home/contact-us');
    }
}

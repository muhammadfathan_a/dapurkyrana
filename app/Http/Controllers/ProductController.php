<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;

use App\Image;
use App\Product;
use App\Category;
use App\Segment;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $product  = DB::table('product')->distinct()->orderBy('created_at', 'DESC')->paginate(10000);
        $category = Category::All();
        $segment  = Segment::All();

        return view('admin/product', [
            'category'  => $category,
            'product'   => $product,
            'segment'   => $segment
        ]);
    }

    public function store(Request $request)
    {
        $filenameWithExt    = $request->file('img_produk')->getClientOriginalName();
        $filename           = pathinfo($filenameWithExt, PATHINFO_FILENAME);
        $extension          = $request->file('img_produk')->getClientOriginalExtension();

            $fileNameToStore = $filename.'_'.time().'.'.$extension;

        $idImage = uniqid();

        Image::create([
            'id_image'      => $idImage,
            'nm_image'      => $request->file('img_produk')->move('product/', $fileNameToStore),
            'path'          => 'public',
            'id_category'   => $request->id_category
        ]);

        Product::create([
            'nm_product'    => $request->nm_produk,
            'id_category'   => $request->id_category,
            'id_segment'    => $request->id_segment,
            'description'   => $request->desc_produk,
            'price'         => $request->harga_produk,
            'id_image'      => $idImage
        ]);

        return redirect('admin/product')->with(
            'status', 
            'Berhasil menambahkan produk ke dalam catalog'
        );
    }

    public function show(Product $product)
    {
        $dataProduct    = Product::findOrFail($product);
        $dataCategory   = Category::all()->where('id_category', $product->id_category);
        $dataImage      = Image::all()->where('id_image', $product->id_image);
        $dataSegment    = Segment::All()->where('id_segment', $product->id_segment);

        return view('admin/detail/product', [
            'product'   => $dataProduct,
            'category'  => $dataCategory,
            'image'     => $dataImage,
            'segment'   => $dataSegment
        ]);
    }

    public function edit(Product $product)
    {
        $dataProduct    = Product::findOrFail($product)->find($product);
        $dataCategory   = Category::all();
        $dataImage      = Image::all()->where('id_image', $product->id_image);
        $dataSegment    = Segment::All();

        return view('admin/edit/product', [
             'product' => $dataProduct,
             'category' => $dataCategory,
             'segment' => $dataSegment,
             'image' => $dataImage
         ]);
    }

    public function update(Request $request, $id)
    {
        $product = Product::all()->where('id_product', $id);
        $idImage = $product->find($id);

        $img = Image::where('id_image', $idImage->id_image)->first();

        Storage::delete($img->nm_image);

        $filenameWithExt    = $request->file('img_produk')->getClientOriginalName();
        $filename           = pathinfo($filenameWithExt, PATHINFO_FILENAME);
        $extension          = $request->file('img_produk')->getClientOriginalExtension();

            $fileNameToStore = $filename.'_'.time().'.'.$extension;

        Product::findOrFail($id)->update([
            'nm_product'    => $request->nm_produk,
            'id_category'   => $request->id_category,
            'id_segment'    => $request->id_segment,
            'description'   => $request->desc_produk,
            'price'         => $request->harga_produk,
            'id_image'      => $idImage->id_image
        ]);

        Image::findOrFail($idImage->id_image)->update([
            'id_image'      => $idImage->id_image,
            'nm_image'      => $request->file('img_produk')->move('product/', $fileNameToStore),
            'path'          => 'public',
            'id_category'   => $request->id_category
        ]);

        return redirect('admin/product')->with('status', 'Berhasil mengubah produk');

    }

    public function destroy(Product $product)
    {
        $idImage = $product->find($product->id_product);

        $img = Image::where('id_image', $idImage->id_image)->first();

            File::delete($img->nm_image);

        Image::destroy($idImage->id_image);
        Product::destroy($idImage->id_product);

        return redirect('admin/product')->with('status', 'Berhasil menghapus produk');
    }

    public function category()
    {
        $product    =   DB::table('product')->distinct()->orderBy('created_at', 'DESC')
                        ->whereNotExists(function ($query) {
                            $query->select(DB::raw(1000))
                                ->from('category')
                                ->whereRaw('product.id_category = category.id_category');
                        })
                        ->paginate(7);

        return view('admin/other/product', ['product' => $product]);
    }

    public function segment()
    {
        $product    =   DB::table('product')->distinct()->orderBy('created_at', 'DESC')
                            ->whereNotExists(function ($query) {
                            $query->select(DB::raw(1000))
                                ->from('segment')
                                ->whereRaw('product.id_segment = segment.id_segment');
                        })
                        ->paginate(7);

        return view('admin/other/segment', ['product' => $product]);
    }
}

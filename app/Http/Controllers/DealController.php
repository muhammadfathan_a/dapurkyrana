<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;

use DB;
use App\Product;
use App\Category;
use App\Image;
use App\Deal;

class DealController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $deal  = DB::table('deal')->distinct()->orderBy('created_at', 'DESC')->paginate(7);
        $category = Category::all();

        return view('admin/deal', ['deal' => $deal, 'category' => $category]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $filenameWithExt    = $request->file('img_deal')->getClientOriginalName();
        $filename           = pathinfo($filenameWithExt, PATHINFO_FILENAME);
        $extension          = $request->file('img_deal')->getClientOriginalExtension();

            $fileNameToStore = $filename.'_'.time().'.'.$extension;

        $idImage = uniqid();

        Image::create([
            'id_image'      => $idImage,
            'nm_image'      => $request->file('img_deal')->move('deal/', $fileNameToStore),
            'path'          => 'public',
            'id_category'   => $request->id_category
        ]);

        deal::create([
            'title_deal'    => $request->title_deal,
            'subtitle_deal' => $request->subtitle_deal,
            'description'   => $request->desc_deal,
            'price'         => $request->harga_deal,
            'id_category'   => $request->id_category,
            'id_image'      => $idImage
        ]);

        return redirect('admin/deal')->with('status', 'Berhasil menambahkan promosi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Deal $deal)
    {
        $dataDeal       = Deal::findOrFail($deal);
        $dataCategory   = Category::all()->where('id_category', $deal->id_category);
        $dataImage      = Image::all()->where('id_image', $deal->id_image);

        return view('admin/detail/deal', [
            'deal' => $dataDeal,
            'category' => $dataCategory,
            'image' => $dataImage
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Deal $deal)
    {
        $dataDeal       = Deal::findOrFail($deal)->find($deal);
        $dataCategory   = Category::all();
        $dataImage      = Image::all()->where('id_image', $deal->id_image)->first();

        return view('admin/edit/deal', [
              'deal' => $dataDeal,
              'category' => $dataCategory,
              'image' => $dataImage
        ]);
        // return $dataImage;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $deal    = Deal::all()->where('id_deal', $id);
        $idImage = $deal->find($id);

        $img = Image::where('id_image', $idImage->id_image)->first();

        Storage::delete($img->nm_image);

        $filenameWithExt    = $request->file('img_deal')->getClientOriginalName();
        $filename           = pathinfo($filenameWithExt, PATHINFO_FILENAME);
        $extension          = $request->file('img_deal')->getClientOriginalExtension();

            $fileNameToStore = $filename.'_'.time().'.'.$extension;

        Deal::findOrFail($id)->update([
            'title_deal'    => $request->title_deal,
            'subtitle_deal' => $request->subtitle_deal,
            'description'   => $request->desc_deal,
            'price'         => $request->harga_deal,
            'id_category'   => $request->id_category,
            'id_image'      => $idImage->id_image
        ]);

        Image::findOrFail($idImage->id_image)->update([
            'id_image'      => $idImage->id_image,
            'nm_image'      => $request->file('img_deal')->move('deal/', $fileNameToStore),
            'path'          => 'public',
            'id_category'   => $request->id_category
        ]);

        return redirect('admin/deal')->with('status', 'Berhasil mengubah promosi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

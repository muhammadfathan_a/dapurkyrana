<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Product;
use App\Category;
use App\Image;

use SoftDeletes;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $product    = DB::table('product')->distinct()
                    ->join('category', 'product.id_category', '=', 'category.id_category')
                    ->get();

        $category   = Category::all();

        return view('admin/category', ['product' => $product, 'category' => $category]);
    }

    // public function create()
    // {
    //     // Hasi Sayang wkwkkwkwkwkwkw
    // }

    public function store(Request $request)
    {
        Category::create([
            'nm_category' => $request->nm_kategori
        ]);
        return redirect('admin/category')->with('status', 'Berhasil menambahkan kategori untuk produk anda');
    }

    public function show(Category $category)
    {
        $category   = Category::findOrFail($category->id_category);
        $idCategory = $category->id_category;

        $nmCategory = Category::All()->where('id_category', $idCategory);

        $product    = Product::all()->where('id_category', $idCategory);

        $image      = Image::all();

        return view('admin/detail/category', [
            'product'   => $product,
            'image'     => $image,
            'nm'        => $nmCategory
        ]);
    }

    // public function edit($id)
    // {
    //     //
    // }

    public function update(Request $request, $id)
    {
        Category::findOrFail($id)->update([
            'id_category'   => $request->id_kategori,
            'nm_category'   => $request->nm_kategori
        ]);

        return redirect('admin/category')->with('status', 'Berhasil mengubah category');
    }

    public function destroy(Category $category)
    {
        // // Mendapatkan id Image dari id Category
        // $idImage    = Image::all()->where('id_category', $category->id_category)->first();

        // // Mengahapus gambar yang category nya dihapus
        // Storage::delete($idImage->nm_image);

        // // Menghapus Kategory
        // Image::destroy($idImage->id_image);

        Category::destroy($category->id_category);

        return redirect('admin/category')->with('status', 'Berhasil Menghapus category');
    }
}

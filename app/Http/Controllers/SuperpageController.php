<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Image;
use App\Product;
use App\About;
use App\Facades\Instagram;

class SuperpageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('admin/dashboard');
        // return view('admin/dashboard')
        //     ->with('user', Instagram::getUser())
        //     ->with('posts', Instagram::getPosts());
    }
 
    public function search(Request $request){
        return view('search')
            ->with('posts', Instagram::getTagPosts($request->tag));
    }

    public function product()
    {
        $category = Category::all();

        return view('admin/product', ['category' => $category]);
    }

    public function deals()
    {
        return view('admin/deal');
    }

    public function popup()
    {
        return view('admin/pop-up');
    }

    public function aboutus()
    {
        $about  = About::all();
        return view('admin/about-us', ['about' => $about]);
    }

    public function callus()
    {
        return view('admin/call-us');
    }
}

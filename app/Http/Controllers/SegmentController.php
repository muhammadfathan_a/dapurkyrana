<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use App\Segment;
use App\Category;
use App\Product;
use App\Image;

class SegmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $segment    = Segment::All();

        return view('admin/segment', compact('segment', $segment));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Segment::create([
            'nm_segment' => $request->nm_segment
        ]);
        return redirect('admin/segment')->with('status', 'Berhasil menambahkan segmen untuk produk anda');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Segment $segment)
    {

        $segment    = Segment::findOrFail($segment->id_segment);
        $idSegment  = $segment->id_segment;

        $nmSegment  = Segment::All()->where('id_segment', $idSegment);

        $product    = Product::all()->where('id_segment', $idSegment);

        $image      = Image::all();

        return view('admin/detail/segment', [
            'product'   => $product,
            'image'     => $image,
            'nm'        => $nmSegment
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

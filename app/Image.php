<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table = 'image';
    protected $primaryKey = 'id_image';
    protected $keyType = 'string';
    protected $fillable = ['id_image','nm_image','path','id_category'];
}

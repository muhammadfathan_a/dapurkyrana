<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'product';
    protected $primaryKey = 'id_product';
    protected $keyType = 'string';

    protected $fillable =   [
                                'nm_product',
                                'id_category',
                                'id_segment',
                                'description',
                                'price',
                                'id_image'
                            ];
}

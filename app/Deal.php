<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deal extends Model
{
    protected $table        = 'deal';
    protected $primaryKey   = 'id_deal';
    protected $keyType      = 'string';

    protected $fillable     =   [
                                    'title_deal',
                                    'subtitle_deal',
                                    'description',
                                    'price',
                                    'id_category',
                                    'id_image'
                                ];
}

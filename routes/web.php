<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Controller for main website
Route::get('/',                                     'HomepageController@home');


// Controller for main website
Route::get('/home',                                 'HomepageController@home');
Route::get('/product',                              'HomepageController@product');
Route::get('/gallery',                              'HomepageController@gallery');
Route::get('/service',                              'HomepageController@service');
Route::get('/blog',                                 'HomepageController@blog');
Route::get('/about',                                'HomepageController@about');
Route::get('/contact_us',                           'HomepageController@contact_us');

// Controller for auth session
Auth::routes();

Route::get('/admin',                                'SuperpageController@index');
Route::get('/admin/dashboard',                      'SuperpageController@index');

Route::get('/admin/product',                        'ProductController@index');
Route::get('/admin/product/detail/{product}',       'ProductController@show');
Route::get('/admin/product/edit/{product}',         'ProductController@edit');
Route::post('/admin/product/update/{product}',      'ProductController@update');
Route::get('/admin/product/delete/{product}',       'ProductController@destroy');
Route::post('/admin/product',                       'ProductController@store');
Route::get('/admin/product/withoutcategory',        'ProductController@category');
Route::get('/admin/product/withoutsegment',         'ProductController@segment');

Route::get('/admin/category',                       'CategoryController@index');
Route::get('/admin/category/detail/{category}',     'CategoryController@show');
Route::get('/admin/category/edit/{category}',       'CategoryController@edit');
Route::post('/admin/category/update/{category}',    'CategoryController@update');
Route::delete('/admin/category/delete/{category}',  'CategoryController@destroy');
Route::post('/admin/category',                      'CategoryController@store');


Route::get('/admin/segment',                        'SegmentController@index');
Route::get('/admin/segment/detail/{segment}',       'SegmentController@show');
Route::get('/admin/segment/edit/{segment}',         'SegmentController@edit');
Route::post('/admin/segment/update/{segment}',      'SegmentController@update');
Route::delete('/admin/segment/delete/{segment}',    'SegmentController@destroy');
Route::post('/admin/segment',                       'SegmentController@store');


Route::get('/admin/deal',                           'DealController@index');
Route::get('/admin/deal/detail/{deal}',             'DealController@show');
Route::get('/admin/deal/edit/{deal}',               'DealController@edit');
Route::post('/admin/deal/update/{deal}',            'DealController@update');
Route::delete('/admin/deal/delete/{deal}',          'DealController@destroy');
Route::post('/admin/deal',                          'DealController@store');


Route::post('/admin/about-us/update/{about}',       'AboutController@update');


Route::get('/admin/pop-up',                         'SuperpageController@popup');
Route::get('/admin/about-us',                       'SuperpageController@aboutus');
Route::get('/admin/call-us',                        'SuperpageController@callus');

Auth::routes();

// Route::group(['middleware' => ['auth']], function(){
//     Route::get('/instagram', 'InstagramController@redirectToInstagramProvider');
//     Route::get('/instagram/callback', 'InstagramController@handleProviderInstagramCallback');
// });

// Route::group(['middleware' => ['auth', 'instagram']], function(){
//     // routes
// });